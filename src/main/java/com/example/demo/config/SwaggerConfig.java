package com.example.demo.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * SwaggerConfig 配置swagger相关的内容，同时添加对Bean Validator注解的支持
 *
 * @author zhaogaz
 * @date 2018/06/11
 */
@Configuration
@EnableSwagger2
@Import({springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration.class})
@Profile("dev")
public class SwaggerConfig {
    /*
     * 配置说明书： 1. 还可以注入 SecurityConfiguration bean来配置安全相关的内容 2. 可以注入多个docket
     * 配置多个卷宗，表明groupName就行，groupName会自动显示在右上角 3. 其余的注释已经卸载哥哥类中了
     */

    private TypeResolver typeResolver;

    @Autowired
    public SwaggerConfig(TypeResolver typeResolver) {
        this.typeResolver = typeResolver;
    }

    /**
     * 初始化卷宗，也就是注入docket对象
     *
     * @return Docket
     */
    @Bean
    public Docket docket() {
        // 通用response
        List<ResponseMessage> globalResponse = new ArrayList<>();
        /*
         * apiInfo 载入api信息 select
         * 返回一个ApiSelectorBuilder控制哪些信息暴露给swagger展示,一种细粒度的控制，直到build结束 apis
         * 指定请求的控制器，有多种方式，可以制定包路径，也可以指定自动，也可以指定某些注解的，
         *
         * directModelSubstitute
         * 直接模型替换，意思是将某些class替换成另外一种class，全局model生效，多用于Date转Long 之类
         * genericModelSubstitutes 泛型转换，非泛型程序会报错,暂时看不出任何效果 alternateTypeRules
         * 自动类型转换，就是上面两个的自定义的类型 useDefaultResponseMessages 使用默认的回复消息，
         * globalResponseMessage 全局消息回复 ignoredParameterTypes
         * 全局忽略参数类型，一旦忽略，再也无法注册上
         *
         * securitySchemes
         * 安全样式，也就是用的什么安全协议，目前用的JWT的token做的安全验证。出现在页面右上角使用，可以借助这个内容做登录设置
         * securityContext 安全上下文，定义具体的验证模式，也就是说什么样的api需要验证权限什么样的不需要
         *
         * enableUrlTemplating 是否开启url模板，就是是否按照官方的一个template 写url
         *
         * globalOperationParameters 全局操作参数，类似于统计网页来源之类的
         *
         * tags 添加一个标签分类，
         */
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).paths(PathSelectors.any())
                .build().pathMapping("/")
                .globalResponseMessage(RequestMethod.GET, globalResponse)
                .globalResponseMessage(RequestMethod.POST, globalResponse)
                .globalResponseMessage(RequestMethod.PUT, globalResponse)
                .globalResponseMessage(RequestMethod.DELETE, globalResponse);
    }

    /**
     * 文档的info 信息
     *
     * @return ApiInfo 对象，用于上文
     */
    private ApiInfo apiInfo() {
        /*
         * title 标题 description 标题描述 version 版本 等等，其他不重要
         */
        return new ApiInfoBuilder().title("EWIS系统API文档").description("记录于2018.06.08").version("1.0").build();
    }


}