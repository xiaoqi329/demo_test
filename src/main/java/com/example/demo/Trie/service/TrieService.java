package com.example.demo.Trie.service;

import com.example.demo.Trie.model.TrieNode;
import com.example.demo.Trie.model.TrieRowView;
import com.example.demo.Trie.model.Trie;
import com.example.demo.mapper.DivisionMapper;
import com.example.demo.mapper.JokMapper;
import com.example.demo.model.Division;
import com.example.demo.model.Jok;
import com.example.demo.utils.MemoryUtil;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrieService {
    private Trie root = null;
    private DivisionMapper divisionMapper;
    private JokMapper jokMapper;

    public TrieService(DivisionMapper divisionMapper, JokMapper jokMapper) {
        this.divisionMapper = divisionMapper;
        this.jokMapper = jokMapper;
    }

    /**
     * 初始化
     */
    public void initTrie() {
        List<Division> list = divisionMapper.getList();
        Trie trie = new Trie();
        for (Division division : list) {
            trie.putWord(division.getId(), division.getName());
        }
        List<Jok> list1 = jokMapper.getList();
        for (Jok jok : list1) {
            trie.putWord(jok.getId(), jok.getContent());
        }
        root = trie;
        System.out.println("占用内存：" + MemoryUtil.getMb(root) + "M");
        System.out.println("占用内存：" + root.getSize() + "M");
    }

    public List<TrieRowView> getListForVal(String val) {
        return root.getListForVal(val);
    }

    public Trie getRoot() {
        return root;
    }
}
