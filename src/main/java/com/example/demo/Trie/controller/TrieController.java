package com.example.demo.Trie.controller;

import com.example.demo.Trie.model.Trie;
import com.example.demo.Trie.model.TrieRowView;
import com.example.demo.Trie.model.TrieView;
import com.example.demo.Trie.service.TrieService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "字典树实例")
@RestController
public class TrieController {
    private TrieService trieService;

    public TrieController(TrieService trieService) {
        this.trieService = trieService;
    }

    @ApiOperation(value = "初始化字典树")
    @GetMapping("/init")
    public void init() {
        trieService.initTrie();
    }

    @ApiOperation(value = "获取root")
    @GetMapping("/root")
    public Trie root() {
        return trieService.getRoot();
    }


    @ApiOperation(value = "获取推荐数据")
    @GetMapping("/get")
    public TrieView get(String val) {
        long startTime = System.currentTimeMillis();
        List<TrieRowView> list = trieService.getListForVal(val);
        long endTime = System.currentTimeMillis();
        System.out.println(endTime - startTime + "毫秒");
        return new TrieView(list, endTime - startTime);
    }

}
