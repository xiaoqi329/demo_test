package com.example.demo.Trie.model;

import com.example.demo.utils.MemoryUtil;

import java.util.List;
import java.util.Map;

public class Trie {

    private TrieNode root;

    private int size;


    public Trie() {
        this.root = new TrieNode();
        this.size = 0;
    }

    public void setSize(int size) {
        this.size = size;
    }

    /**
     * 插入文字
     *
     * @param id   文字来源id（对应的数据来源的主键id）
     * @param word 文字
     */
    public void putWord(Long id, String word) {
        TrieNode cNode = this.root;
        char[] chars = word.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            Map<Character, TrieNode> sonsMap = cNode.getSonsMap();
            char aChar = chars[i];
            boolean end = i == chars.length - 1;
            if (!sonsMap.keySet().contains(aChar)) {
                //最后一个字母依然没有 size +1
                if (end) {
                    size++;
                }
                //没有下一个字母，保存后返回下一子节点
                cNode = cNode.addSonForChar(aChar, id, end);
            } else {
                cNode = sonsMap.get(aChar);
                if (end) {
                    sonsMap.get(aChar).setEnd(end);
                    sonsMap.get(aChar).setId(id);
                }
            }
        }
    }

    /**
     * 根据
     *
     * @param val
     * @return
     */
    public List<TrieRowView> getListForVal(String val) {
        return root.getListForVal(val);
    }

    public int getSize() {
        return MemoryUtil.getMb(this.root);
    }
}
