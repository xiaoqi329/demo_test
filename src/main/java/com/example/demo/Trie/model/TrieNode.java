package com.example.demo.Trie.model;

import java.util.*;

public class TrieNode {

    private char val;
    /**
     * 对应数据来源的主键
     */
    private Long id;

    private boolean isEnd;
    /**
     * 点击次数
     */
    private int clickNum;

    private List<TrieNode> sons = new ArrayList<>();

    private Map<Character, TrieNode> sonsMap = new HashMap<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getClickNum() {
        return clickNum;
    }

    public void setClickNum(int clickNum) {
        this.clickNum = clickNum;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public Map<Character, TrieNode> getSonsMap() {
        return sonsMap;
    }

    public void setSonsMap(Map<Character, TrieNode> sonsMap) {
        this.sonsMap = sonsMap;
    }

    public TrieNode(Character character, boolean isEnd) {
        this.val = character;
        this.isEnd = isEnd;
    }

    public TrieNode() {
    }

    public char getVal() {
        return val;
    }

    public void setVal(char val) {
        this.val = val;
    }

    public TrieNode getTrieNodeFromSons(Character character) {
        return sonsMap.get(character);

    }

    public List<TrieNode> getSons() {
        return sons;
    }

    public void setSons(List<TrieNode> sons) {
        this.sons = sons;
    }

    public TrieNode addSonForChar(char aChar, Long id, boolean isEnd) {
        TrieNode newNode = new TrieNode(aChar, isEnd);
        if (isEnd) {
            newNode.setId(id);
        }
        this.getSons().add(newNode);
        //维护sonMap
        this.sonsMap.put(aChar, newNode);
        return newNode;
    }


    public List<TrieRowView> getListForVal(String val) {
        List<TrieRowView> reList = new ArrayList<>();
        TrieNode node = this;
        char[] chars = val.toCharArray();
        for (char aChar : chars) {
            Map<Character, TrieNode> sonsMap = node.getSonsMap();
            TrieNode trieNode = sonsMap.get(aChar);
            if (trieNode != null) {
                node = trieNode;
            } else {
                node = null;
                break;
            }
        }
        if (node != null) {
            boolean isEnd = node.isEnd;
            if (isEnd) {
                reList.add(new TrieRowView(node.getId(), val));
            }
            List<TrieRowView> sons = new ArrayList<>();
            getAllSonsIsEnd(node, val, sons);
            reList.addAll(sons);
        }
        return reList;
    }

    private Collection<? extends TrieRowView> getAllSonsIsEnd(TrieNode node, String val, List<TrieRowView> reList) {
        List<TrieNode> sonsForNode = node.getSons();
        if (sonsForNode.size() > 0) {
            for (TrieNode son : sonsForNode) {
                if (son.isEnd) {
                    reList.add(new TrieRowView(son.getId(), val + son.getVal()));
                }
                getAllSonsIsEnd(son, val + son.getVal(), reList);
            }
        }
        return reList;

    }

}
