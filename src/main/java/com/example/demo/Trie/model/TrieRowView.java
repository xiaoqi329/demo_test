package com.example.demo.Trie.model;

public class TrieRowView {
    private Long id;
    private String val;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public TrieRowView(Long id, String val) {
        this.id = id;
        this.val = val;
    }
}
