package com.example.demo.Trie.model;

import java.util.List;

public class TrieView {
    private List<TrieRowView> list;
    private long totalTime;

    public TrieView(List<TrieRowView> list, long totalTime) {
        this.list = list;
        this.totalTime = totalTime;
    }

    public List<TrieRowView> getList() {
        return list;
    }

    public void setList(List<TrieRowView> list) {
        this.list = list;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }
}
