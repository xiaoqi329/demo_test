package com.example.demo;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.apache.ibatis.session.AutoMappingUnknownColumnBehavior;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@SpringBootApplication(scanBasePackages = {"com.example.demo"})
@MapperScan("com.example.demo")
@EnableTransactionManagement
public class DemoApplication {
    /**
     * data source from druid
     *
     * @return DataSource
     */
    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSource dataSourceDev() {
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * mybatis sqlSession bean
     *
     * @return sql session
     * @throws Exception ???
     */
    @Bean
    public SqlSessionFactory sqlSessionFactoryBean(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:/mybatis/**/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }

    /**
     * mybatis config
     *
     * @return config bean
     */
    @Bean
    public ConfigurationCustomizer mybatisConfigurationCustomizer() {
        return configuration -> {
            configuration.setCacheEnabled(false);
            //映射到未知列直接fail
            configuration.setAutoMappingUnknownColumnBehavior(AutoMappingUnknownColumnBehavior.FAILING);
            //自动把下划线转为驼峰命名
            configuration.setMapUnderscoreToCamelCase(true);

        };
    }

    /**
     * 事物支持bean
     *
     * @param dataSource 注入对应的数据源
     * @return 事物管理
     */
    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        System.out.println("success");
    }

}
