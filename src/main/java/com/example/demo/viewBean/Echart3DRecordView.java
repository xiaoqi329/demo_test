package com.example.demo.viewBean;

public class Echart3DRecordView {

    private Object x;

    private Integer xNum;

    private Object y;

    private Integer yNum;

    private Number z;

    public Integer getxNum() {
        return xNum;
    }

    public void setxNum(Integer xNum) {
        this.xNum = xNum;
    }

    public Integer getyNum() {
        return yNum;
    }

    public void setyNum(Integer yNum) {
        this.yNum = yNum;
    }


    public Object getX() {
        return x;
    }

    public void setX(Object x) {
        this.x = x;
    }

    public Object getY() {
        return y;
    }

    public void setY(Object y) {
        this.y = y;
    }

    public Number getZ() {
        return z;
    }

    public void setZ(Number z) {
        this.z = z;
    }

    public Echart3DRecordView(Object x, Object y, Number z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Echart3DRecordView() {
    }
}
