package com.example.demo.viewBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Echart3DView {
    /**
     * 数据列表
     */
    private List<Echart3DRecordView> list = new ArrayList<>();

    private String xName;
    private String yName;
    private String zName;

    public static Echart3DView newNull() {
        return new Echart3DView();
    }

    public String getxName() {
        return xName;
    }

    public void setxName(String xName) {
        this.xName = xName;
    }

    public String getyName() {
        return yName;
    }

    public void setyName(String yName) {
        this.yName = yName;
    }

    public String getzName() {
        return zName;
    }

    public void setzName(String zName) {
        this.zName = zName;
    }

    /**
     * x轴列表
     */
    public List<Object> getXList() {
        return list.stream().map(Echart3DRecordView::getX).distinct().collect(Collectors.toList());
    }

    /**
     * y轴列表
     */
    public List<Object> getYList() {
        return list.stream().map(Echart3DRecordView::getY).distinct().collect(Collectors.toList());
    }

    /**
     * z轴列表
     */
    public List<Number> getZList() {
        return list.stream().map(Echart3DRecordView::getZ).distinct().collect(Collectors.toList());
    }

    public List<Echart3DRecordView> getList() {
        Map<Object, Integer> mapX = list2Map(getXList());
        Map<Object, Integer> mapY = list2Map(getYList());
        for (Echart3DRecordView echart3DRecordView : list) {
            echart3DRecordView.setxNum(mapX.get(echart3DRecordView.getX()));
            echart3DRecordView.setyNum(mapY.get(echart3DRecordView.getY()));
        }
        return list;
    }

    private <T> Map<T, Integer> list2Map(List<T> xList) {
        Map<T, Integer> map = new HashMap<>();
        for (int i = 0; i < xList.size(); i++) {
            T t = xList.get(i);
            map.put(t, i);
        }
        return map;
    }

    public Number getMaxZ() {
        List<Number> collect = getZList().stream().sorted().collect(Collectors.toList());
        return collect.get(collect.size() - 1);
    }

    public void setList(List<Echart3DRecordView> list) {
        this.list = list;
    }

    public Echart3DView(List<Echart3DRecordView> list, String xName, String yName, String zName) {
        this.list = list;
        this.xName = xName;
        this.yName = yName;
        this.zName = zName;
    }

    public Echart3DView() {
    }
}
