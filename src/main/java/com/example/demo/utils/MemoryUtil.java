package com.example.demo.utils;

import com.example.demo.bean.Test;
import org.apache.lucene.util.RamUsageEstimator;

public class MemoryUtil {

    /**
     * 不好使
     *
     * @param obj
     * @return
     */
    public static int getMb(Object obj) {

        if (obj == null) {
            return 0;
        }
        //计算指定对象本身在堆空间的大小，单位字节
        long byteCount = RamUsageEstimator.shallowSizeOf(obj);
        if (byteCount == 0) {
            return 0;
        }
        double oneMb = 1 * 1024 * 1024;
        if (byteCount < oneMb) {
            return 1;
        }

        Double v = Double.valueOf(byteCount) / oneMb;
        return v.intValue();
    }
}