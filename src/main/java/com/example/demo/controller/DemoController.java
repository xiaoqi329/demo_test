package com.example.demo.controller;

import com.example.demo.bean.Test;
import com.example.demo.excel.ExcelUtil;
import com.example.demo.excel.model.ExcelColInfo;
import com.example.demo.excel.model.TestDemo;
import com.example.demo.excel.template.TemplateConfigs;
import com.example.demo.excel.template.TemplateFactory;
import com.example.demo.excel.template.model.ITemplateConfig;
import com.example.demo.excel.template.model.TemplateColConfig;
import com.example.demo.excel.template.model.TemplateConfig;
import com.example.demo.excel.writer.WriterFactory;
import com.example.demo.excel.read.ReadFactory;
import com.example.demo.excel.enums.CustomCellType;
import com.example.demo.excel.read.model.*;
import com.example.demo.excel.read.view.ReadExcelDataView;
import com.example.demo.excel.writer.model.*;
import com.example.demo.service.PeopleService;
import com.example.demo.viewBean.Echart3DRecordView;
import com.example.demo.viewBean.Echart3DView;
import com.github.kevinsawicki.http.HttpRequest;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Demo 使用
 *
 * @author qi
 */
@Controller
public class DemoController {


    private PeopleService peopleService;

    public DemoController(PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public String index(String signature, String timestamp, String nonce, String echostr) {
        return echostr;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @ResponseBody
    public String test() {
        Map<String, Object> res = new HashMap<>();
        res.put("code", 200);
        res.put("msg", "success");
        System.out.println("============================访问=======================================");
        Test test = new Test(1L, "张三");
        List<Test> list = Arrays.asList(test);
        Map<String, List<Test>> collect = list.stream().collect(Collectors.groupingBy(test1 -> test.getId() + "-" + test.getName()));
        peopleService.test();
        return res.toString();
    }

    @RequestMapping(value = "/3dPic", method = RequestMethod.GET)
    public String hello(Model model) {
        return "3d";
    }

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model, String code) {
        HttpRequest httpRequest = HttpRequest.get("https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code");
        model.addAttribute("name", name);
        return "greeting";
    }

    private List<String> strings1 = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
    private List<String> strings2 = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");

    @GetMapping("/3d")
    @ResponseBody
    public Echart3DView test3d() {
        Echart3DView ev = new Echart3DView();
        ev.setxName("x轴");
        ev.setyName("y轴");
        ev.setzName("z轴");
        //制造假数据
        List<Echart3DRecordView> list = new ArrayList<>();
        int size = strings2.size();
        for (int j = 0; j < strings1.size(); j++) {
            String s1 = strings1.get(j);
            for (int i = 0; i < strings2.size(); i++) {
                String s2 = strings2.get(i);
                if (i < size / 2 && j < size / 2) {
                    list.add(new Echart3DRecordView(s1, s2, j < i ? j : i));
                } else if (i >= size / 2 && j >= size / 2) {
                    list.add(new Echart3DRecordView(s1, s2, i < j ? size - j : size - i));
                } else if (j >= size / 2) {
                    int min = i > size - j ? size - j : i;
                    list.add(new Echart3DRecordView(s1, s2, min));
                } else {
                    int min = j > size - i ? size - i : j;
                    list.add(new Echart3DRecordView(s1, s2, min));
                }
            }
        }
        ev.setList(list);
        return ev;
    }

    @GetMapping("/size")
    @ResponseBody
    public void size() {
        List<Test> testList = new ArrayList<>();
        for (int i = 0; i < 1000000L; i++) {
            Test test = new Test((long) i, "test" + i);
            testList.add(test);
        }
        System.out.println(testList.size());
    }

    @RequestMapping(value = "/magnify", method = RequestMethod.GET)
    public String magnify() {
        return "magnify";
    }

    @RequestMapping("/tire")
    public String trie() {
        return "tire";
    }


}

