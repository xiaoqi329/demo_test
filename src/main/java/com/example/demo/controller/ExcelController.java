package com.example.demo.controller;

import com.example.demo.bean.Test;
import com.example.demo.excel.ExcelUtil;
import com.example.demo.excel.enums.CustomCellType;
import com.example.demo.excel.model.ExcelColInfo;
import com.example.demo.excel.model.TestDemo;
import com.example.demo.excel.read.ReadFactory;
import com.example.demo.excel.read.model.*;
import com.example.demo.excel.read.view.ReadExcelDataView;
import com.example.demo.excel.template.TemplateConfigs;
import com.example.demo.excel.template.TemplateFactory;
import com.example.demo.excel.writer.WriterFactory;
import com.example.demo.excel.writer.model.*;
import com.example.demo.service.PeopleService;
import com.example.demo.viewBean.Echart3DRecordView;
import com.example.demo.viewBean.Echart3DView;
import com.github.kevinsawicki.http.HttpRequest;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Demo 使用
 *
 * @author qi
 */
@Controller
public class ExcelController {


    private PeopleService peopleService;

    public ExcelController(PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @RequestMapping("/excel")
    public String excel() {
        return "excel";
    }

    @RequestMapping("/excel/file")
    @ResponseBody
    public ReadExcelDataView excelFile(MultipartFile file) {
        List<ExcelColInfo> objects = Arrays.asList(
                new ReadExcelColConfig("机场名称", "name", false, 50, CustomCellType.STRING),
                new ReadExcelColConfig("英文", "engName", false, 10, CustomCellType.STRING),
                new ReadExcelColConfig("国家", "country", false, 10, CustomCellType.STRING),
                new ReadExcelColConfig("城市", "city", false, 10, CustomCellType.STRING),
                new ReadExcelColConfig("经度", "x", false, null, CustomCellType.DOUBLE),
                new ReadExcelColConfig("纬度", "y", false, null, CustomCellType.DOUBLE),
                new ReadExcelColConfig("级别", "level", true, 10, CustomCellType.STRING),
                new ReadExcelColConfig("预计飞行里程", "licheng", true, null, CustomCellType.DOUBLE));
        IReadConfig<TestDemo> config = new ReadExcelConfig<>(true, true, objects, new TestDemo());
        ReadExcelDataView<TestDemo> dataView = new ReadFactory<>(config, file).createDataView();
        System.out.println(dataView);
        return dataView;
    }

    @RequestMapping("/excel/file1")
    @ResponseBody
    public ReadExcelDataView excelFile1(MultipartFile file) {
        List<ExcelColInfo> objects = Arrays.asList(
                new ReadExcelColConfig("机场名称", "name", false, 50, CustomCellType.STRING),
                new ReadExcelColConfig("英文", "engName", false, 10, CustomCellType.STRING),
                new ReadExcelColConfig("国家", "country", false, 10, CustomCellType.STRING),
                new ReadExcelColConfig("城市", "city", false, 10, CustomCellType.STRING),
                new ReadExcelColConfig("经度", "x", false, null, CustomCellType.DOUBLE),
                new ReadExcelColConfig("纬度", "y", false, null, CustomCellType.DOUBLE),
                new ReadExcelColConfig("级别", "level", true, 10, CustomCellType.STRING),
                new ReadExcelColConfig("预计飞行里程", "licheng", true, null, CustomCellType.DOUBLE));
        IReadConfig<TestDemo> config = new ReadExcelPrimaryConfig<>(true, true, objects, new TestDemo(), new PrimaryConfig("id", true));
        ReadExcelDataView<TestDemo> dataView = new ReadFactory<>(config, file).createDataView();
        System.out.println(dataView);
        return dataView;
    }

    @RequestMapping("/excel/file2")
    @ResponseBody
    public ReadExcelDataView excelFile2(MultipartFile file) {
        //列配置列表，必须按顺序排
        ReadExcelColConfig son = new ReadExcelColConfig("产品编号", "number", false, 50, CustomCellType.STRING);
        ReadExcelColConfig parent = new ReadExcelColConfig("父节点", "pid", false, 36, CustomCellType.STRING);
        List<ExcelColInfo> colConfigs = Arrays.asList(
                son, new ReadExcelColConfig("产品名称", "name", true, 30, CustomCellType.STRING),
                new ReadExcelColConfig("结构层次", "structure", true, 30, CustomCellType.STRING), parent,
                new ReadExcelColConfig("ATA章节", "ata", true, 10, CustomCellType.STRING),
                new ReadExcelColConfig("单机数量", "num", true, null, CustomCellType.INT),
                new ReadExcelColConfig("产品类型", "type", true, 30, CustomCellType.STRING),
                new ReadExcelColConfig("产品型号", "model", true, 30, CustomCellType.STRING),
                new ReadExcelColConfig("生产/研制单位", "productionUnit", true, 30, CustomCellType.STRING),
                new ReadExcelColConfig("备注", "remark", true, 500, CustomCellType.STRING));
        //总配置文件
        ReadExcelDataView view = new ReadFactory<>(new ReadExcelPrimaryTreeConfig<>(son, parent, colConfigs, new TestAirDemo(), false, true, new PrimaryConfig("id", true)), file).createDataView();
        return view;
    }

    @RequestMapping("/excel/writer")
    public void writer(HttpServletResponse response) {
        List<WriterExcelColConfig> colConfigs = Arrays.asList(new WriterExcelColConfig("id", "id", null),
                new WriterExcelColConfig("名字", "name", 10));
        List<Test> datas = Arrays.asList(new Test(1L, "中文圣诞节覅士大夫撒的发生"), new Test(2L, "222222222222222222222222222222222222222222222222222222222222222"));
        IWriterConfig<Test> config = new ExcelSimpleConfig<>(12, null, 16, 33, "名字", true, datas, "sheetName", colConfigs);
        WriterFactory<Test> testWriterFactory = new WriterFactory<>(config);
        Workbook wb = testWriterFactory.createWb();
        ExcelUtil.exportExcel(wb, "名字", response);
    }

    @RequestMapping("/excel/writer2")
    public void writer2(HttpServletResponse response) {
        List<WriterExcelColConfig> colConfigs = Arrays.asList(new WriterExcelColConfig("id", "id", null), new WriterExcelColConfig("名字", "name", 10));
        List<Test> datas = Arrays.asList(new Test(1L, "中文圣诞节覅士大夫撒的发生"), new Test(2L, "222222222222222222222222222222222222222222222222222222222222222"));
        List<WriterSheetConfig<Test>> writerSheetConfigs = Arrays.asList(new WriterSheetConfig<Test>(datas, colConfigs, "sheet1", true), new WriterSheetConfig<Test>(datas, colConfigs, "sheet2", true));
        IWriterConfig<Test> config = new ExcelMultiSheetConfig<>(12, null, 14, 33, "名字", writerSheetConfigs);
        WriterFactory<Test> testWriterFactory = new WriterFactory<>(config);
        Workbook wb = testWriterFactory.createWb();
        ExcelUtil.exportExcel(wb, "名字", response);
    }

    @RequestMapping("/excel/template")
    public void template(HttpServletResponse response) {
        TemplateFactory<TestDemo> factory = new TemplateFactory<>(TemplateConfigs.getAir());
        Workbook wb = factory.getWb();
        ExcelUtil.exportExcel(wb, "模板文件", response);

    }

    @RequestMapping("/excel/template/read")
    @ResponseBody
    public ReadExcelDataView excelF(MultipartFile file) {
        TemplateFactory<TestDemo> factory = new TemplateFactory<>(TemplateConfigs.getAir());
        ReadExcelDataView dataView = factory.getDataView(file);
        return dataView;
    }

    @RequestMapping("/excel/color")
    public void testcolor(HttpServletResponse response) {
        Workbook wb = ExcelUtil.getWbForColor();
        ExcelUtil.exportExcel(wb, "模板文件", response);

    }


}

