package com.example.demo.excel.writer.model;

import com.example.demo.excel.writer.service.IWriterService;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * 导出配置接口
 *
 * @param <T>
 */
public interface IWriterConfig<T> {
    /**
     * 提供配置对应的业务类
     *
     * @return service
     */
    IWriterService<T> getService();
}
