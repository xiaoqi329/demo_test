package com.example.demo.excel.writer.model;

import java.util.List;

/**
 * 每一个sheet的配置
 */
public class WriterSheetConfig<T> {
    /**
     * 传入的数据列表
     */
    private List<T> datas;

    /**
     * 每个sheet对应的列配置
     */
    private List<WriterExcelColConfig> colConfigs;
    /**
     * sheetName
     */
    private String sheetName;
    /**
     * 是否有序号列
     */
    private boolean hasColIndex;

    public boolean isHasColIndex() {
        return hasColIndex;
    }

    public void setHasColIndex(boolean hasColIndex) {
        this.hasColIndex = hasColIndex;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public List<WriterExcelColConfig> getColConfigs() {
        return colConfigs;
    }

    public void setColConfigs(List<WriterExcelColConfig> colConfigs) {
        this.colConfigs = colConfigs;
    }

    public WriterSheetConfig(List<T> datas, List<WriterExcelColConfig> colConfigs, String sheetName, boolean hasColIndex) {
        this.datas = datas;
        this.colConfigs = colConfigs;
        this.sheetName = sheetName;
        this.hasColIndex = hasColIndex;
    }
}
