package com.example.demo.excel.writer.model;

/**
 * excel表格的基本配置
 */
public class WriterExcelColConfig {
    /**
     * 表头名称
     */
    private String headName;
    /**
     * 对应对象的属性名称
     */
    private String property;
    /**
     * 列宽 不能大于 255
     */
    private Integer columnWidth;


    public Integer getColumnWidth() {
        return columnWidth;
    }

    public void setColumnWidth(Integer columnWidth) {
        columnWidth = limit255Char(columnWidth);
        this.columnWidth = columnWidth;
    }

    public String getHeadName() {
        return headName;
    }

    public void setHeadName(String headName) {
        this.headName = headName;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public WriterExcelColConfig(String headName, String property, Integer columnWidth) {
        this.headName = headName;
        this.property = property;
        columnWidth = limit255Char(columnWidth);
        this.columnWidth = columnWidth;
    }

    private Integer limit255Char(Integer columnWidth) {
        //excel 列宽最大为255个字符，这里做限制
        if (columnWidth != null && columnWidth > 255) {
            columnWidth = 255;
        }
        return columnWidth;
    }

}
