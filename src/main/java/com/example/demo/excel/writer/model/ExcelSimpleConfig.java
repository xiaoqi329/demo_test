package com.example.demo.excel.writer.model;

import com.example.demo.excel.ExcelUtil;
import com.example.demo.excel.model.ExcelConfig;
import com.example.demo.excel.writer.service.IWriterService;
import com.example.demo.excel.writer.service.WriterServiceImpl;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.thymeleaf.util.StringUtils;

import java.util.List;

/**
 * 单个sheet单表 配置
 *
 * @param <T>
 */
public class ExcelSimpleConfig<T> extends ExcelConfig implements IWriterConfig<T> {

    /**
     * 是否有序号（默认有）
     */
    private boolean hasColIndex = true;
    /**
     * 导出的数据列表
     */
    private List<T> datas;
    /**
     * 导出sheet名称
     */
    private String sheetName = "sheet1";
    /**
     * 列配置列表
     */
    private List<WriterExcelColConfig> colConfigs;

    private HSSFCellStyle cellStyle;


    public HSSFCellStyle getCellStyle() {
        return cellStyle;
    }

    public void setCellStyle(HSSFCellStyle cellStyle) {
        this.cellStyle = cellStyle;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public List<WriterExcelColConfig> getColConfigs() {
        return colConfigs;
    }

    public void setColConfigs(List<WriterExcelColConfig> colConfigs) {
        this.colConfigs = colConfigs;
    }

    public boolean isHasColIndex() {
        return hasColIndex;
    }

    public void setHasColIndex(boolean hasColIndex) {
        this.hasColIndex = hasColIndex;
    }


    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }


    /**
     * 构造配置信息
     *
     * @param hasColIndex 是否有 序号
     * @param datas       数据
     * @param fontSize    字号 可空
     * @param rowHeight   行高 可空
     * @param columnWidth 列宽 可空
     * @param sheetName   sheet名
     * @param title       文件名
     * @param colConfigs  单列配置
     */
    public ExcelSimpleConfig(Integer fontSize, String fontName, int rowHeight, int columnWidth, String title, boolean hasColIndex, List<T> datas, String sheetName, List<WriterExcelColConfig> colConfigs) {
        super(fontSize, fontName, rowHeight, columnWidth, title);
        this.hasColIndex = hasColIndex;
        this.datas = datas;
        this.sheetName = StringUtils.isEmpty(sheetName) ? this.sheetName : sheetName;
        this.colConfigs = colConfigs;
    }

    private Integer limit255Char(Integer columnWidth) {
        //excel 列宽最大为255个字符，这里做限制
        if (columnWidth != null && columnWidth > 255) {
            columnWidth = 255;
        }
        return columnWidth;
    }

    @Override
    public IWriterService<T> getService() {
        return new WriterServiceImpl<T>();
    }
}
