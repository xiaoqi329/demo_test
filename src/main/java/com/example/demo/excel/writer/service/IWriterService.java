package com.example.demo.excel.writer.service;

import com.example.demo.excel.writer.model.IWriterConfig;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * 获取工作簿接口
 */
public interface IWriterService<T> {
    /**
     * 获取配置所生成的工作簿
     *
     * @param config 配置
     * @return 工作簿
     */
    Workbook getWb(IWriterConfig<T> config);
}
