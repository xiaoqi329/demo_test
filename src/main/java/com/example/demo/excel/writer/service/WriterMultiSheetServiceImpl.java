package com.example.demo.excel.writer.service;

import com.example.demo.excel.writer.model.ExcelMultiSheetConfig;
import com.example.demo.excel.writer.model.IWriterConfig;
import com.example.demo.excel.writer.model.WriterSheetConfig;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

import static com.example.demo.excel.ExcelUtil.createSheetForWriter;
import static com.example.demo.excel.ExcelUtil.createWb;

/**
 * 生成多sheet业务类
 */
public class WriterMultiSheetServiceImpl<T> extends WriterServiceImpl<T> {

    @Override
    public Workbook getWb(IWriterConfig config) {
        ExcelMultiSheetConfig<T> multiSheetConfig = (ExcelMultiSheetConfig<T>) config;
        List<WriterSheetConfig<T>> sheetConfigs = multiSheetConfig.getSheetConfigs();
        Workbook wb = createWb();
        for (WriterSheetConfig<T> sheetConfig : sheetConfigs) {
            createSheetForWriter(sheetConfig.getSheetName(), sheetConfig.isHasColIndex(), sheetConfig.getColConfigs(), multiSheetConfig, sheetConfig.getDatas(), wb);
        }
        return wb;
    }

}
