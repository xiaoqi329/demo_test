package com.example.demo.excel.writer.model;

import com.example.demo.excel.ExcelUtil;
import com.example.demo.excel.model.ExcelConfig;
import com.example.demo.excel.writer.service.IWriterService;
import com.example.demo.excel.writer.service.WriterMultiSheetServiceImpl;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * 多个sheet配置
 */
public class ExcelMultiSheetConfig<T> extends ExcelConfig implements IWriterConfig<T> {

    /**
     * 每个sheet的配置
     */
    private List<WriterSheetConfig<T>> sheetConfigs;

    public List<WriterSheetConfig<T>> getSheetConfigs() {
        return sheetConfigs;
    }

    public void setSheetConfigs(List<WriterSheetConfig<T>> sheetConfigs) {
        this.sheetConfigs = sheetConfigs;
    }

    public ExcelMultiSheetConfig(Integer fontSize, String fontName, int rowHeight, int columnWidth, String title, List<WriterSheetConfig<T>> sheetConfigs) {
        super(fontSize, fontName, rowHeight, columnWidth, title);
        this.sheetConfigs = sheetConfigs;
    }

    @Override
    public IWriterService<T> getService() {
        return new WriterMultiSheetServiceImpl<T>();
    }
}
