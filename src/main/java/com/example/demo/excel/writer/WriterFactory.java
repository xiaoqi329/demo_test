package com.example.demo.excel.writer;

import com.example.demo.excel.writer.model.IWriterConfig;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

public class WriterFactory<T> {

    private IWriterConfig<T> config;

    public WriterFactory(IWriterConfig<T> config) {
        this.config = config;
    }

    /**
     * 创建wb
     *
     * @return wb
     */
    public Workbook createWb() {
        return config.getService().getWb(config);
    }
}
