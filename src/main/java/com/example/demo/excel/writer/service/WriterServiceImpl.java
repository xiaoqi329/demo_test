package com.example.demo.excel.writer.service;

import com.example.demo.excel.ExcelUtil;
import com.example.demo.excel.writer.model.ExcelSimpleConfig;
import com.example.demo.excel.writer.model.IWriterConfig;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Workbook;

import static com.example.demo.excel.ExcelUtil.*;

/**
 * 简单输出业务类 （单模型列表转为单个sheet表格）
 */
public class WriterServiceImpl<T> implements IWriterService<T> {

    @Override
    public Workbook getWb(IWriterConfig config) {
        ExcelSimpleConfig<T> simpleConfig = (ExcelSimpleConfig<T>) config;
        return getWbForSimple(simpleConfig);
    }

    /**
     * 获取工作簿(简单配置的)
     *
     * @param config 配置
     * @param <T>    泛型
     * @return wb
     * @author qi
     */
    public static <T> Workbook getWbForSimple(ExcelSimpleConfig<T> config) {
        //创建工作簿
        Workbook wb = createWb();
        //创建sheet
        createSheetForWriter(config.getSheetName(), config.isHasColIndex(), config.getColConfigs(), config, config.getDatas(), wb);
        return wb;
    }

}
