package com.example.demo.excel;

import com.example.demo.excel.enums.CustomCellType;
import com.example.demo.excel.enums.ExceptionType;
import com.example.demo.excel.model.ExcelColInfo;
import com.example.demo.excel.model.ExcelConfig;
import com.example.demo.excel.read.ReadExcelException;
import com.example.demo.excel.read.model.PrimaryConfig;
import com.example.demo.excel.read.view.ReadExcelDataMsgView;
import com.example.demo.excel.read.view.ReadExcelDataView;
import com.example.demo.excel.template.model.TemplateColConfig;
import com.example.demo.excel.template.model.TemplateConfig;
import com.example.demo.excel.writer.model.WriterExcelColConfig;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;


/**
 * 读取excel 工具类
 * （暂时只提供读取excel07及以上版本）
 *
 * @author qi
 */
public class ExcelUtil {

    /**
     * 校验是否有错误
     *
     * @param dataView view
     * @param <T>      泛型
     */
    public static <T> void validateHasException(ReadExcelDataView<T> dataView) {
        if (dataView.getMsgs().size() > 0) {
            System.out.println(dataView.getExceptionMsg());
            throw new ReadExcelException(dataView.getExceptionMsg());
        }
    }

    /**
     * 获取table中的数据
     *
     * @param <T>        泛型
     * @param sheet      sheet
     * @param model      返回的模型
     * @param colConfigs 列配置
     * @param rowLength  表的高度
     * @param xIndex     x开始的索引
     * @param yIndex     y开始的索引
     * @return view
     */
    public static <T> ReadExcelDataView<T> getTableData(Sheet sheet, T model, List<ExcelColInfo> colConfigs, int rowLength, int xIndex, int yIndex) {
        //初始化返回对象
        ReadExcelDataView<T> view = new ReadExcelDataView<T>();
        //遍历获取所有的cell 中的内容以及检验数据填写是否正常
        for (int i = yIndex; i < rowLength; i++) {
            T modelOfRow = getNewModel(model);
            Row row = sheet.getRow(i);
            if (validateRowIsNull(row, view, i + 1)) {
                for (int j = xIndex; j < colConfigs.size() + xIndex; j++) {
                    //校验是否是空行
                    Cell cell = row.getCell(j);
                    ExcelColInfo colConfig = colConfigs.get(j - xIndex);
                    //获取每个cell中的数据
                    getCellDataAndValidVal(colConfig, cell, view, modelOfRow, i + 1, colConfig.getName());
                }

            }
            view.getDatas().add(modelOfRow);
        }
        return view;
    }

    /**
     * 检验是否是空行
     *
     * @param <T>  泛型
     * @param row  行
     * @param view view
     * @param i    第多少行
     * @return
     */
    public static <T> boolean validateRowIsNull(Row row, ReadExcelDataView<T> view, int i) {
        if (row == null) {
            view.getMsgs().add(new ReadExcelDataMsgView("行数据都为空", i, "", ExceptionType.ROW_NOT_NULL));
            return false;
        }
        return true;
    }

    /**
     * 获取每个cell中的数据以及检验数据填写是否符合要求
     *
     * @param colConfig 列配置
     * @param cell      cell
     * @param view      返回的view信息
     * @param model     此行数据对应的model
     * @param rows      第几行
     * @param headName  列名
     * @param <T>       泛型
     */
    private static <T> void getCellDataAndValidVal(ExcelColInfo colConfig, Cell cell, ReadExcelDataView<T> view, T model, int rows, String headName) {
        CustomCellType customCellType = colConfig.getCustomCellType();
        //获取每个cell中的数据
        Object returnObj = getCellDataObject(colConfig, cell, view, rows, headName, customCellType);
        //反射设置属性值
        try {
            if (returnObj != null) {
                Field declaredField = model.getClass().getDeclaredField(colConfig.getProperty());
                declaredField.setAccessible(true);
                declaredField.set(model, returnObj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取下拉框中的数据对应的主键数据（这里的不仅仅是主键数据，也可以为其他属性，TemplateColConfig 中readProperty 配置）
     *
     * @param returnObj excel select中读取的数据
     * @param colConfig 列配置
     * @return 对应的属性值
     */
    private static Object getCellValForSelectType(Object returnObj, ExcelColInfo colConfig) {
        TemplateColConfig templateColConfig = (TemplateColConfig) colConfig;
        List selects = templateColConfig.getSelects();
        for (int i = 0; i < selects.size(); i++) {
            Object select = selects.get(i);
            try {
                Field declaredField = select.getClass().getDeclaredField(((TemplateColConfig) colConfig).getDisplayProperty());
                declaredField.setAccessible(true);
                Object displayVal = declaredField.get(select);
                if (returnObj.equals(displayVal)) {
                    if (hasField(select.getClass(), ((TemplateColConfig) colConfig).getReadProperty())) {
                        Field readField = select.getClass().getDeclaredField(((TemplateColConfig) colConfig).getReadProperty());
                        readField.setAccessible(true);
                        return readField.get(select);
                    }
                }
                return i;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;

    }

    private static boolean hasField(Class c, String fieldName) {
        Field[] fields = c.getDeclaredFields();
        for (Field f : fields) {
            if (fieldName.equals(f.getName())) {
                return true;
            }
        }
        return false;
    }


    /**
     * 获取每个cell中的数据
     *
     * @param colConfig 列配置
     * @param cell      cell
     * @param view      返回的view信息
     * @param rows      第几行
     * @param headName  列名
     * @param <T>       泛型
     * @return data
     */
    public static <T> Object getCellDataObject(ExcelColInfo colConfig, Cell cell, ReadExcelDataView<T> view, int rows, String headName, CustomCellType customCellType) {
        Object returnObj = null;
        switch (customCellType) {
            case STRING:
                //获取字符串类型
                returnObj = getStringValForCell(cell, colConfig, view, rows, headName);
                break;
            case DOUBLE:
                //获取数字类型
                returnObj = getDoubleValForCell(cell, colConfig, view, rows, headName);
                break;
            case DATE:
                //日期类型
                returnObj = getDateValForCell(cell, colConfig, view, rows, headName);
                break;
            case INT:
                //int
                returnObj = getIntValForCell(cell, colConfig, view, rows, headName);
                break;
            case SELECT:
            case ENUM:
                returnObj = getSelectValForCell(cell, colConfig, view, rows, headName);
                break;
            case TIME:
                returnObj = getTimeValForCell(cell, colConfig, view, rows, headName);
                break;
        }
        return returnObj;
    }

    private static <T> Object getTimeValForCell(Cell cell, ExcelColInfo colConfig, ReadExcelDataView<T> view, int rows, String headName) {
        //if (validateNotNull(cell, colConfig, view, rows, headName)) return null;
        if (cell == null) return null;
        //校验字符串形式
        if (org.apache.poi.ss.usermodel.CellType.STRING.equals(cell.getCellType()) && isValidDate(cell.getStringCellValue()) == null) {
            view.getMsgs().add(new ReadExcelDataMsgView("不是合法日期", rows, headName, ExceptionType.NOT_DATE));
        }
        if (org.apache.poi.ss.usermodel.CellType.STRING.equals(cell.getCellType())) {
            return isValidDate(cell.getStringCellValue());
        } else {
            return cell.getLocalDateTimeCellValue();
        }
    }

    private static <T> Object getSelectValForCell(Cell cell, ExcelColInfo colConfig, ReadExcelDataView<T> view, int rows, String headName) {
        String returnObj = getStringValForCell(cell, colConfig, view, rows, headName);
        //如果是下拉选择框的话单独处理
        if (!StringUtils.isEmpty(returnObj)) {
            Object cellValForSelectType = getCellValForSelectType(returnObj, colConfig);
            return cellValForSelectType;
        }
        return returnObj;
    }

    /**
     * 获取 日期类型的单元格的值
     *
     * @param cell      单元格
     * @param colConfig 列配置
     * @param view      view
     * @param rows      第几行
     * @param headName  列名
     * @param <T>       泛型
     * @author qi
     */
    private static <T> Date getDateValForCell(Cell cell, ExcelColInfo colConfig, ReadExcelDataView<T> view, int rows, String headName) {
        if (validateNotNull(cell, colConfig, view, rows, headName)) return null;
        if (cell == null) return null;
        //校验字符串形式
        if (org.apache.poi.ss.usermodel.CellType.STRING.equals(cell.getCellType()) && isValidDate(cell.getStringCellValue()) == null) {
            view.getMsgs().add(new ReadExcelDataMsgView("不是合法日期", rows, headName, ExceptionType.NOT_DATE));
        }
        if (org.apache.poi.ss.usermodel.CellType.STRING.equals(cell.getCellType())) {
            return isValidDate(cell.getStringCellValue());
        } else {
            return cell.getDateCellValue();
        }

    }

    /**
     * 获取数字类型的cell的值
     *
     * @param cell      单元格
     * @param colConfig 列配置
     * @param view      view
     * @param rows      第多少行
     * @param headName  列名
     * @param <T>       泛型
     * @author qi
     */
    private static <T> Integer getIntValForCell(Cell cell, ExcelColInfo colConfig, ReadExcelDataView<T> view, int rows, String headName) {
        if (validateNumber(cell, colConfig, view, rows, headName)) return null;
        //如果以文本形式出现处理
        if (org.apache.poi.ss.usermodel.CellType.STRING.equals(cell.getCellType())) {
            return Integer.valueOf(cell.getStringCellValue());
        } else {
            return new Double(cell.getNumericCellValue() + "").intValue();
        }
    }

    private static <T> boolean validateNumber(Cell cell, ExcelColInfo colConfig, ReadExcelDataView<T> view, int rows, String headName) {
        if (validateNotNull(cell, colConfig, view, rows, headName)) return true;
        //如果没有必须非空这里会出现空值，验空
        if (cell == null) return true;
        //如果这里出现文本形式校验
        if (CellType.STRING.equals(cell.getCellType()) && !isInteger(cell.getStringCellValue())) {
            view.getMsgs().add(new ReadExcelDataMsgView("不是合法数字", rows, headName, ExceptionType.NOT_NUMBER));
            return true;
        }
        //检验大小
        double val = 0.0;
        if (CellType.STRING.equals(cell.getCellType())) {
            val = new Double(cell.getStringCellValue());
        } else if (CellType.NUMERIC.equals(cell.getCellType())) {
            val = cell.getNumericCellValue();
        }
        return (colConfig.getMin() != null && val < colConfig.getMin()) || (colConfig.getMax() != null && val > colConfig.getMax());
    }

    /**
     * 获取数字类型的cell的值
     *
     * @param cell      单元格
     * @param colConfig 列配置
     * @param view      view
     * @param rows      第多少行
     * @param headName  列名
     * @param <T>       泛型
     * @author qi
     */
    private static <T> Double getDoubleValForCell(Cell cell, ExcelColInfo colConfig, ReadExcelDataView<T> view, int rows, String headName) {
        if (validateNumber(cell, colConfig, view, rows, headName)) return null;
        //如果以文本形式出现处理
        if (org.apache.poi.ss.usermodel.CellType.STRING.equals(cell.getCellType())) {
            return Double.valueOf(cell.getStringCellValue());
        } else {
            return cell.getNumericCellValue();
        }
    }

    /**
     * 判断是否是数字
     *
     * @param str s
     * @return boolean
     */
    private static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    /**
     * 判断是否是日期 是返回date 不是返回null
     *
     * @param strDate 日期字符串
     * @return booena
     */
    private static Date isValidDate(String strDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2018-02-29会被接受，并转换成2018-03-01
            format.setLenient(false);
            date = format.parse(strDate);
            //判断传入的yyyy年-MM月-dd日 字符串是否为数字
            String[] sArray = strDate.split("-");
            for (String s : sArray) {
                boolean isNum = s.matches("[0-9]+");
                //+表示1个或多个（如"3"或"225"），*表示0个或多个（[0-9]*）（如""或"1"或"22"），?表示0个或1个([0-9]?)(如""或"7")
                if (!isNum) {
                    return null;
                }
            }
        } catch (Exception e) {
            return null;
        }
        return date;
    }

    /**
     * 统一校验非空要求
     *
     * @param cell      单元格
     * @param colConfig 列配置
     * @param view      view
     * @param rows      第多少行
     * @param headName  列名
     * @param <T>       泛型
     * @return 是否符合
     * @author qi
     */
    private static <T> boolean validateNotNull(Cell cell, ExcelColInfo colConfig, ReadExcelDataView<T> view, int rows, String headName) {
        boolean canBlank = colConfig.isCanBlank();
        if (!canBlank && (cell == null || cell.getCellType().equals(org.apache.poi.ss.usermodel.CellType.BLANK))) {
            view.getMsgs().add(new ReadExcelDataMsgView("不能为空", rows, headName, ExceptionType.NOT_NULL));
            return true;
        }
        return false;
    }

    /**
     * 获取字符类型的cell的值
     *
     * @param cell      cell
     * @param colConfig 列配置
     * @param view      view返回数据
     * @param rows      第多少行
     * @param headName  列名
     * @param <T>       泛型
     * @author qi
     */
    private static <T> String getStringValForCell(Cell cell, ExcelColInfo colConfig, ReadExcelDataView<T> view, int rows, String headName) {
        if (validateNotNull(cell, colConfig, view, rows, headName)) return "";
        //如果没有必须非空这里会出现空值，验空
        if (cell == null) return "";
        String returnStr = null;
        //如果填写的是一串数字，会返回数字类型在这里会出现问题,日期类型无法区分，这里cxcel 直接给单元格文本类型可解决此问题
        if (org.apache.poi.ss.usermodel.CellType.NUMERIC.equals(cell.getCellType())) {
            returnStr = cell.getNumericCellValue() + "";
        } else {
            returnStr = cell.getStringCellValue();
        }
        //验长度
        if (returnStr.length() > colConfig.getMaxLength()) {
            view.getMsgs().add(new ReadExcelDataMsgView("长度超出最大长度：" + colConfig.getMaxLength(), rows, headName, ExceptionType.BEYOND_MAXLENGTH));
        }
        return returnStr;
    }

    public static void validateHasSheet(Workbook workbook) {
        int numberOfSheets = workbook.getNumberOfSheets();
        if (numberOfSheets == 0) {
            throw new ReadExcelException("必须要有一个sheet!");
        }
    }

    /**
     * 获取工作波
     *
     * @param inputStream 输入流
     * @return 工作簿
     * @author qi
     */
    public static Workbook getWorkbook(InputStream inputStream) {
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (workbook != null) {
                    workbook.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return workbook;
    }


    public static <T> ReadExcelDataView<T> validateFile(MultipartFile file) {
        //判空
        if (file.isEmpty()) {
            return new ReadExcelDataView<T>(Collections.singletonList(new ReadExcelDataMsgView("上传文件为空", ExceptionType.FILE_NOT_NULL)));
        }
        //校验文件格式
        String originalFilename = file.getOriginalFilename();
        boolean is03Excell = originalFilename.matches("^.+\\.(?i)(xls)$");
        //校验是否是03版本
        if (is03Excell) {
            return new ReadExcelDataView<T>(Collections.singletonList(new ReadExcelDataMsgView("上传文件格式错误", ExceptionType.FILE_FORMAT_FAULT)));
        }
        boolean is07MoreExcel = originalFilename.matches("^.+\\.(?i)(xlsx)$");
        //校验是否是07级以上版本
        if (!is07MoreExcel) {
            return new ReadExcelDataView<T>(Collections.singletonList(new ReadExcelDataMsgView("不支持2003及以下版本", ExceptionType.NOT_SUPPORT)));
        }
        return null;
    }


    /**
     * 设置主键值
     *
     * @param <T>           泛型
     * @param model         模型
     * @param primaryConfig 主键配置信息
     * @return 生成的主键
     */
    public static <T> String setPrimaryVal(T model, PrimaryConfig primaryConfig) {
        String id = UUID.randomUUID().toString();
        if (primaryConfig != null && primaryConfig.isAutoGenerate()) {
            try {
                String primaryProperty = primaryConfig.getPrimaryProperty();
                Field declaredField = model.getClass().getDeclaredField(primaryProperty);
                declaredField.setAccessible(true);
                declaredField.set(model, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return id;
    }

    /**
     * 获取新的model对象
     *
     * @param model model
     * @param <T>   泛型
     * @return model
     */
    public static <T> T getNewModel(T model) {
        try {
            return (T) model.getClass().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Workbook createWb() {
        return new XSSFWorkbook();
    }

    /**
     * 创建sheet(一个sheet)
     *
     * @param sheetName      sheetName
     * @param hasColumnIndex 是否有序号列
     * @param colConfigs     列配置列表
     * @param baseConfig     基本配置
     * @param datas          数据列表
     * @param wb             工作簿
     * @param <T>            泛型
     * @author qi
     */
    public static <T> void createSheetForWriter(String sheetName, boolean hasColumnIndex, List<WriterExcelColConfig> colConfigs, ExcelConfig baseConfig, List<T> datas, Workbook wb) {
        //创建sheet
        Sheet wbSheet = wb.createSheet(sheetName);
        //设置列宽
        setColWidth(wbSheet, hasColumnIndex, colConfigs, baseConfig);
        int startIndex = hasColumnIndex ? 1 : 0;
        //创建表头
        createHeadRow(baseConfig, wbSheet, startIndex, colConfigs, hasColumnIndex);
        //创建表体
        createBodyRows(baseConfig, wbSheet, startIndex, hasColumnIndex, colConfigs, datas);
    }

    /**
     * 创建表体部分
     *
     * @param config      配置
     * @param wbSheet     sheet
     * @param startIndex  开始索引
     * @param <T>         泛型
     * @param colConfigs  列配置列表
     * @param hasColIndex 是否有序号列
     * @param datas       数据列表
     */
    private static <T> void createBodyRows(ExcelConfig config, Sheet wbSheet, int startIndex, boolean hasColIndex, List<WriterExcelColConfig> colConfigs, List<T> datas) {
        CellStyle commonCellStyle = config.getCommonCellStyle(wbSheet.getWorkbook());
        commonCellStyle.setFillForegroundColor(IndexedColors.WHITE1.getIndex());
        for (int j = 0; j < datas.size(); j++) {
            T data = datas.get(j);
            //创建行
            Row rowBody = createRow(wbSheet, j + 1, config.getRowHeight());
            if (hasColIndex) {
                createCell(commonCellStyle, rowBody, 0, (j + 1) + "");
            }
            for (int i = startIndex; i < colConfigs.size() + startIndex; i++) {
                WriterExcelColConfig colConfig = colConfigs.get(i - startIndex);
                createCell(commonCellStyle, rowBody, i, getPropertyValue(data, colConfig.getProperty()));
            }
        }
    }

    /**
     * 创建表头row
     *
     * @param config      配置
     * @param wbSheet     sheet
     * @param startIndex  开始索引
     * @param colConfigs  列配置列表
     * @param hasColIndex 是否有序号列
     */
    private static void createHeadRow(ExcelConfig config, Sheet wbSheet, int startIndex, List<WriterExcelColConfig> colConfigs, boolean hasColIndex) {
        Row rowHead = createRow(wbSheet, 0, config.getRowHeight());
        CellStyle headCellStyle = config.getHeadCellStyle(wbSheet.getWorkbook());
        headCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        if (hasColIndex) {
            createCell(headCellStyle, rowHead, 0, "序号");
        }
        for (int i = startIndex; i < colConfigs.size() + startIndex; i++) {
            WriterExcelColConfig colConfig = colConfigs.get(i - startIndex);
            createCell(headCellStyle, rowHead, i, colConfig.getHeadName());
        }
    }

    /**
     * 获取文字样式
     *
     * @param fontSize 文字大小
     * @param fontName 文字烈性名称
     * @param wb       工作簿
     * @param bold     是否加粗
     * @param italic   是否 倾斜
     * @return font
     */
    public static Font createFont(int fontSize, String fontName, Workbook wb, boolean bold, boolean italic) {
        Font font = wb.createFont();
        font.setFontHeightInPoints((short) fontSize);
        font.setFontName(fontName);
        font.setBold(bold);
        font.setItalic(italic);
        return font;
    }

    /**
     * 获取通用的单元格样式
     *
     * @param config 配置
     * @param wb     wb
     * @return 单元格样式
     */
    private static CellStyle getCommonCellStyle(ExcelConfig config, Workbook wb) {
        //创建字体样式
        Font font = createFont(config.getFontSize(), config.getFontName(), wb, true, false);
        CellStyle cellStyle = createCellStyle(wb, HorizontalAlignment.CENTER, VerticalAlignment.TOP, true, font, null);
        DataFormat format = wb.createDataFormat();
        cellStyle.setDataFormat(format.getFormat("@"));
        return cellStyle;
    }

    /**
     * 创建一行
     *
     * @param height  行高
     * @param wbSheet sheet对象
     * @return row
     */
    public static Row createRow(Sheet wbSheet, int rowIndex, int height) {
        Row rowHead = wbSheet.createRow(rowIndex);
        rowHead.setHeightInPoints(height);
        return rowHead;
    }

    /**
     * 获取属性值
     *
     * @param data     bean
     * @param property 属性名称
     * @param <T>      泛型
     * @return 属性值
     */
    private static <T> String getPropertyValue(T data, String property) {
        try {
            Field declaredField = data.getClass().getDeclaredField(property);
            declaredField.setAccessible(true);
            Object content = declaredField.get(data);
            return content != null ? content.toString() : "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }

    /**
     * 创建cell
     *
     * @param cellStyle 样式
     * @param row       行
     * @param i         第几个单元格
     * @param value     内容
     */
    public static void createCell(CellStyle cellStyle, Row row, int i, String value) {
        //单元格全部设置为String 类型，避免数字提示问题(暂时未实现，都不起作用)
        Cell cell = row.createCell(i, CellType.STRING);
        cell.setCellStyle(cellStyle);
        RichTextString richTextString = new XSSFRichTextString(value);
        cell.setCellValue(richTextString);
    }

    /**
     * 设置表格列宽
     *
     * @param wbSheet     sheet
     * @param hasColIndex 是否有序号列
     * @param <T>         泛型
     * @param colConfigs  列配置
     * @param baseConfig  统一配置
     * @author qi
     */
    private static <T> void setColWidth(Sheet wbSheet, boolean hasColIndex, List<WriterExcelColConfig> colConfigs, ExcelConfig baseConfig) {
        //设置行高
        wbSheet.setDefaultRowHeightInPoints(baseConfig.getRowHeight());
        int startIndex = 0;
        if (hasColIndex) {
            //有序号，设置序号列宽度
            wbSheet.setColumnWidth(0, 8 * 256);
            startIndex = 1;
        }
        //设置其他列宽
        for (int i = startIndex; i < colConfigs.size() + startIndex; i++) {
            WriterExcelColConfig colConfig = colConfigs.get(i - startIndex);
            wbSheet.setColumnWidth(i, (colConfig.getColumnWidth() == null ? baseConfig.getColumnWidth() : colConfig.getColumnWidth()) * 256);
        }
    }


    /**
     * 设置 填写数据的列的所有单元格的默认宽高和样式
     *
     * @param hasColIndex 是否有序号
     * @param colInfoList 列信息列表
     * @param sheet       sheet
     */
    public static void setDefaultCellWidthAndHeightAndStyle(boolean hasColIndex, List<ExcelColInfo> colInfoList, Sheet sheet, ExcelConfig config) {
        int startIndex = 0;
        if (hasColIndex) {
            //有序号，设置序号列宽度
            sheet.setColumnWidth(0, 8 * 256);
            //获取数据去样式
            CellStyle dataCellStyle = getDataCellStyleForCellType(sheet.getWorkbook(), CustomCellType.INT);
            sheet.setDefaultColumnStyle(0, dataCellStyle);
            startIndex = 1;
        }
        //设置其他列宽
        for (int i = startIndex; i < colInfoList.size() + startIndex; i++) {
            ExcelColInfo colConfig = colInfoList.get(i - startIndex);
            sheet.setColumnWidth(i, (colConfig.getColumnWidth() == null ? config.getColumnWidth() : colConfig.getColumnWidth()) * 256);
            sheet.setDefaultRowHeightInPoints((short) config.getRowHeight());
            //获取数据去样式
            CellStyle dataCellStyle = getDataCellStyleForCellType(sheet.getWorkbook(), colConfig.getCustomCellType());
            sheet.setDefaultColumnStyle(i, dataCellStyle);
        }
    }

    /**
     * 获取数据单元格样式
     *
     * @param wb             工作簿
     * @param customCellType 单元格类型 暂时未使用
     * @return 单元格样式
     */
    private static CellStyle getDataCellStyleForCellType(Workbook wb, CustomCellType customCellType) {
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setWrapText(true);
        //CreationHelper createHelper = wb.getCreationHelper();
        //cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("@"));
        return cellStyle;
    }

    /**
     * 如果列的类型是下拉选择框，设置下拉
     *
     * @param index     行的第一个索引
     * @param sheet     sheet
     * @param colConfig 列配置
     */
    public static <T> void setColCellSelect(int index, Sheet sheet, TemplateColConfig<T> colConfig) {
        if (CustomCellType.SELECT.equals(colConfig.getCustomCellType())) {
            XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
            List<T> selects = colConfig.getSelects();
            //获取下拉框中显示的属性值列表
            String[] selectNames = getSelectNames(selects, colConfig);
            XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(selectNames);
            CellRangeAddressList addressList = new CellRangeAddressList(2, SpreadsheetVersion.EXCEL2007.getMaxRows() - 1, index , index);
            XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
            validation.setSuppressDropDownArrow(true);
            // Note this extra method call. If this method call is omitted, or if the
            // boolean value false is passed, then Excel will not validate the value the
            // user enters into the cell.
            validation.setShowErrorBox(true);
            sheet.addValidationData(validation);
        }
    }

    /**
     * 获取下拉框中的内容
     *
     * @param selects   下拉框的所有元素列表
     * @param colConfig 列配置
     * @param <T>       泛型
     * @return names
     */
    private static <T> String[] getSelectNames(List<T> selects, TemplateColConfig<T> colConfig) {
        List<String> names = new ArrayList<>();
        for (T selectRow : selects) {
            try {
                Field declaredField = selectRow.getClass().getDeclaredField(colConfig.getDisplayProperty());
                declaredField.setAccessible(true);
                names.add(declaredField.get(selectRow) + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return names.toArray(new String[names.size()]);
    }

    //数字转字母 1-26 ： A-Z
    private static String numberToLetter(int num) {
        if (num <= 0) {
            return null;
        }
        String letter = "";
        num--;
        do {
            if (letter.length() > 0) {
                num--;
            }
            letter = ((char) (num % 26 + (int) 'A')) + letter;
            num = (int) ((num - num % 26) / 26);
        } while (num > 0);

        return letter;
    }

    /**
     * 设置列的批注信息
     *
     * @param sheet     sheet
     * @param colConfig 列配置
     * @param rowIndex  行索引
     * @param colIndex  列索引
     */
    public static void setHeadColComment(Sheet sheet, ExcelColInfo colConfig, int rowIndex, int colIndex) {
        //获取列的提示信息
        String info = getInfoForColConfig(colConfig);
        if (!StringUtils.isEmpty(info)) {
            addComment(sheet, rowIndex, colIndex, info);
        }
    }

    /**
     * 给单元格添加批注信息
     *
     * @param sheet    sheet
     * @param rowIndex 行索引
     * @param colIndex 列索引
     * @param info     信息
     */
    private static void addComment(Sheet sheet, int rowIndex, int colIndex, String info) {
        Row row = sheet.getRow(rowIndex);
        Cell cell = row.getCell(colIndex);
        addComment(sheet, info, cell);
    }

    /**
     * 给单元格添加批注信息
     *
     * @param sheet sheet
     * @param info  批注信息内容
     * @param cell  单元格
     */
    private static void addComment(Sheet sheet, String info, Cell cell) {
        Drawing<?> drawing = sheet.createDrawingPatriarch();
        CreationHelper factory = sheet.getWorkbook().getCreationHelper();
        ClientAnchor anchor = factory.createClientAnchor();
        Comment comment1 = drawing.createCellComment(anchor);
        RichTextString str1 = factory.createRichTextString(info);
        comment1.setString(str1);
        //comment1.setAuthor("Apache POI");
        cell.setCellComment(comment1);
    }

    /**
     * 获取列的提示信息
     *
     * @param colConfig 列配置
     * @return str
     */
    private static String getInfoForColConfig(ExcelColInfo colConfig) {
        CustomCellType customCellType = colConfig.getCustomCellType();
        switch (customCellType) {
            case STRING:
                return getInfoForString(colConfig);
            case DOUBLE:
                return getInfoForNumber(colConfig);
            case SELECT:
                return getInfoForSelect(colConfig);
            case DATE:
                return getInfoForDate(colConfig);
            case INT:
                return getInfoForNumber(colConfig);
            default:
                return "";
        }

    }

    private static String getInfoForDate(ExcelColInfo colConfig) {
        StringBuilder sb = new StringBuilder();
        int index = 1;
        sb.append(index).append(".请输入合法的日期");
        index++;
        if (!colConfig.isCanBlank()) {
            sb.append(index).append(".非空\n");
        }

        return sb.toString();
    }

    private static String getInfoForSelect(ExcelColInfo colConfig) {
        StringBuilder sb = new StringBuilder();
        int index = 1;
        sb.append(index).append(".请下拉选择待选项\n");
        index++;
        if (!colConfig.isCanBlank()) {
            sb.append(index).append(".非空\n");
        }

        return sb.toString();
    }

    /**
     * 数字类型提示信息
     *
     * @param colConfig 列配置
     * @return str
     */
    private static String getInfoForNumber(ExcelColInfo colConfig) {
        StringBuilder sb = new StringBuilder();
        int index = 1;
        if (!colConfig.isCanBlank()) {
            sb.append(index).append(".非空\n");
            index++;
        }
        if (colConfig.getMin() != null) {
            sb.append(index).append(".最小值：").append(colConfig.getMin()).append("\n");
            index++;
        }
        if (colConfig.getMax() != null) {
            sb.append(index).append(".最大值：").append(colConfig.getMin());
        }
        return sb.toString();
    }

    /**
     * 文本提示信息
     *
     * @param colConfig 列配置
     * @return str
     */
    private static String getInfoForString(ExcelColInfo colConfig) {
        StringBuilder sb = new StringBuilder();
        int index = 1;
        if (!colConfig.isCanBlank()) {
            sb.append(index).append(".非空\n");
            index++;
        }
        sb.append(index).append(".最大字节数：").append(colConfig.getMaxLength());
        return sb.toString();
    }


    /**
     * 创建合并后的单元格
     *
     * @param y1             开始行索引
     * @param y2             结束行索引
     * @param x1             开始列索引
     * @param x2             结束列索引
     * @param sheet          sheet
     * @param row            行
     * @param customCellType 类型
     * @param content        单元格内容
     * @param cellStyle      单元格样式
     */
    public static void createMergeCell(int y1, int y2, int x1, int x2, Sheet sheet, Row row, CustomCellType customCellType, String content, CellStyle cellStyle) {
        Cell cell = row.createCell(x1);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(new XSSFRichTextString(content));
        sheet.addMergedRegion(new CellRangeAddress(y1, y2, x1, x2));
    }

    /**
     * 设置单元格样式为文本形式
     *
     * @param wb        工作簿
     * @param cellStyle 单元格类型
     */
    public static void setCellStringType(Workbook wb, CellStyle cellStyle) {
        CreationHelper createHelper = wb.getCreationHelper();
        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("@"));
    }

    /**
     * 导出excel
     *
     * @param fileName 生成excel文件的文件名
     * @param response resp
     */

    public static void exportExcel(Workbook wb, String fileName, HttpServletResponse response) {
        fileName += ".xlsx";
        OutputStream output = null;
        try {
            output = response.getOutputStream();
            response.reset();
            response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1"));
            response.setContentType("application/msexcel");
            response.setContentType("APPLICATION/OCTET-STREAM;charset=UTF-8");//设置类型
            response.setHeader("Cache-Control", "no-cache");//设置头
            wb.write(output);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
                wb.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static Workbook getWbForColor() {
        Workbook wb = createWb();
        Sheet name = wb.createSheet("name");
        IndexedColors[] values = IndexedColors.values();
        int index = 0;
        int row = 0;
        for (IndexedColors value : values) {
            CellStyle cellStyle = wb.createCellStyle();
            cellStyle.setFillForegroundColor(value.getIndex());
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            Row row1 = name.getRow(row);
            if (row1 == null) {
                row1 = name.createRow(row);
            }
            Cell cell = row1.createCell(index % 10);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(value.name());
            row = index / 10;
            index++;

        }

        row += 3;
        BorderStyle[] values1 = BorderStyle.values();
        for (int i = 0; i < values1.length; i++) {
            if (i % 10 == 0) {
                row++;
            }
            Row row1 = name.getRow(row);
            if (row1 == null) {
                row1 = name.createRow(row);
            }
            CellStyle cellStyle = wb.createCellStyle();
            cellStyle.setFillForegroundColor(IndexedColors.TAN.getIndex());
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            cellStyle.setBorderLeft(values1[i]);
            cellStyle.setBorderRight(values1[i]);
            cellStyle.setBorderTop(values1[i]);
            cellStyle.setBorderBottom(values1[i]);
            Cell cell = row1.createCell(i % 10);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(values1[i].name());
        }
        return wb;
    }

    /**
     * 创建 单元格样式（尽量少创建样式，每创建一个样式都会占用空间，样式数量有上限好像2万多）
     *
     * @param workbook            工作簿
     * @param horizontalAlignment 水平
     * @param verticalAlignment   垂直
     * @param wrapText            是否超出文字隐藏
     * @param font                字体样式
     * @param indexOfColors       颜色样式索引 参见枚举IndexedColors
     * @return cellStyle
     */
    public static CellStyle createCellStyle(Workbook workbook, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, boolean wrapText, Font font, Short indexOfColors) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(horizontalAlignment);
        cellStyle.setVerticalAlignment(verticalAlignment);
        cellStyle.setWrapText(wrapText);
        cellStyle.setFont(font);
        if (indexOfColors != null) {
            cellStyle.setFillForegroundColor(indexOfColors);
        }
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        return cellStyle;
    }

    /**
     * 创建sheet
     *
     * @param wb        工作簿
     * @param sheetName sheetName
     * @return sheet
     */
    public static Sheet createSheet(Workbook wb, String sheetName) {
        return wb.createSheet(sheetName);

    }

    /**
     * 限制每列数据的输入格式
     *
     * @param sheet         sheet
     * @param colConfigList 列配置信息
     * @param config        基本配置信息
     */
    public static void setLimitForCols(Sheet sheet, ExcelConfig config, List<ExcelColInfo> colConfigList) {
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
        for (int i = 0; i < colConfigList.size(); i++) {
            ExcelColInfo excelColInfo = colConfigList.get(i);
            CustomCellType customCellType = excelColInfo.getCustomCellType();
            XSSFDataValidationConstraint xssfDataValidationConstraint = null;
            //错误时的提示信息,待区分不同类型提示信息
            String msg = "填写信息有误，请参照表头批注信息！";
            switch (customCellType) {
                case INT:
                    xssfDataValidationConstraint = setNumberLimitForCol(dvHelper, excelColInfo, XSSFDataValidationConstraint.ValidationType.INTEGER, Integer.MIN_VALUE + "", Integer.MAX_VALUE + "");
                    break;
                case DOUBLE:
                    //这里最大值最小值默认 除以1000,是因为太大excel不支持，不知道具体多少实验过后除以1000是可以的
                    xssfDataValidationConstraint = setNumberLimitForCol(dvHelper, excelColInfo, XSSFDataValidationConstraint.ValidationType.DECIMAL, Double.MIN_VALUE / 1000 + "", Double.MAX_VALUE / 1000 + "");
                    break;
                case DATE:
                    xssfDataValidationConstraint = setDateLimitForCol(dvHelper, excelColInfo);
                    break;
                case TIME:
                    xssfDataValidationConstraint = setTimeLimitForCol(dvHelper, excelColInfo);
                    break;
                case SELECT:
                    xssfDataValidationConstraint = setSelectLimitForCol(dvHelper, excelColInfo);
                    break;
                case ENUM:
                    xssfDataValidationConstraint = setEnumLimitForCol(dvHelper, excelColInfo);
                    break;
                default:
                    xssfDataValidationConstraint = setStringLimitForCol(dvHelper, excelColInfo);

            }
            //列索引
            int colIndex = i + (config.isHasColIndex() ? 1 : 0);
            CellRangeAddressList addressList = new CellRangeAddressList(config.getDataStartRowIndex(), SpreadsheetVersion.EXCEL2007.getMaxRows() - 1, colIndex, colIndex);
            XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(xssfDataValidationConstraint, addressList);
            validation.createErrorBox("提示", msg);
            validation.setShowErrorBox(true);
            sheet.addValidationData(validation);
        }
    }

    /**
     * 限制string类型
     *
     * @param dvHelper     dh
     * @param excelColInfo info
     * @return
     */
    private static XSSFDataValidationConstraint setStringLimitForCol(XSSFDataValidationHelper dvHelper, ExcelColInfo excelColInfo) {
        return (XSSFDataValidationConstraint) dvHelper.createTextLengthConstraint(DataValidationConstraint.OperatorType.BETWEEN, excelColInfo.isCanBlank() ? "0" : "1", excelColInfo.getMaxLength() + "");
    }

    /**
     * 限制只能选择下拉
     *
     * @param dvHelper     dv
     * @param excelColInfo info
     * @return
     */
    private static <T> XSSFDataValidationConstraint setSelectLimitForCol(XSSFDataValidationHelper dvHelper, ExcelColInfo excelColInfo) {
        TemplateColConfig<T> templateColConfig = (TemplateColConfig) excelColInfo;
        //获取显示的列表
        String[] selectNames = getSelectNames(templateColConfig.getSelects(), templateColConfig);
        DataValidationConstraint explicitListConstraint = dvHelper.createExplicitListConstraint(selectNames);
        return (XSSFDataValidationConstraint) explicitListConstraint;
    }

    /**
     * 限制(枚举)只能选择下拉
     *
     * @param dvHelper     dv
     * @param excelColInfo info
     * @return
     */
    private static <T> XSSFDataValidationConstraint setEnumLimitForCol(XSSFDataValidationHelper dvHelper, ExcelColInfo excelColInfo) {
        TemplateColConfig<T> templateColConfig = (TemplateColConfig) excelColInfo;
        //获取显示的列表
        String[] selectNames = getSelectNames(templateColConfig.getSelects(), templateColConfig);
        DataValidationConstraint explicitListConstraint = dvHelper.createExplicitListConstraint(selectNames);
        return (XSSFDataValidationConstraint) explicitListConstraint;
    }

    /**
     * 限制时间类型
     *
     * @param dvHelper     dh
     * @param excelColInfo info
     * @return
     */
    private static XSSFDataValidationConstraint setTimeLimitForCol(XSSFDataValidationHelper dvHelper, ExcelColInfo excelColInfo) {
        return (XSSFDataValidationConstraint) dvHelper.createTimeConstraint(DataValidationConstraint.OperatorType.BETWEEN, "Time(00,00,00)", "Time(24,00,00)");
    }

    /**
     * 限制日期类型
     *
     * @param dvHelper
     * @param excelColInfo
     * @return
     */
    private static XSSFDataValidationConstraint setDateLimitForCol(XSSFDataValidationHelper dvHelper, ExcelColInfo excelColInfo) {
        return (XSSFDataValidationConstraint) dvHelper.createDateConstraint(DataValidationConstraint.OperatorType.BETWEEN, "Date(1900,01,01)", "Date(2099,12,30)", "yyyy/MM/dd");
    }

    /**
     * 限制数字类型
     *
     * @param dvHelper     dvHelper
     * @param excelColInfo 列信息
     * @return 限制 如果无限制
     */
    private static XSSFDataValidationConstraint setNumberLimitForCol(XSSFDataValidationHelper dvHelper, ExcelColInfo excelColInfo, Integer validationType, String defaultMin, String defaultMax) {
        //验证数字大小,有上下限限制
        if (excelColInfo.getMax() != null && excelColInfo.getMin() != null) {
            return (XSSFDataValidationConstraint) dvHelper.createNumericConstraint(validationType, XSSFDataValidationConstraint.OperatorType.BETWEEN,
                    excelColInfo.getMin() + "", excelColInfo.getMax() + "");
        } else if (excelColInfo.getMax() != null && excelColInfo.getMin() == null) {
            //只有上限
            return (XSSFDataValidationConstraint) dvHelper.createNumericConstraint(validationType, XSSFDataValidationConstraint.OperatorType.LESS_OR_EQUAL,
                    excelColInfo.getMax() + "", null);
        } else if (excelColInfo.getMin() != null && excelColInfo.getMax() == null) {
            //只有下限
            return (XSSFDataValidationConstraint) dvHelper.createNumericConstraint(validationType, XSSFDataValidationConstraint.OperatorType.GREATER_OR_EQUAL,
                    excelColInfo.getMin() + "", null);
        }
        //没有限制时候限制int类型范围
        return (XSSFDataValidationConstraint) dvHelper.createNumericConstraint(validationType, XSSFDataValidationConstraint.OperatorType.BETWEEN,
                defaultMin, defaultMax);
    }

    /**
     * 创建表体部分
     *
     * @param wb
     * @param sheet
     * @param colConfigs
     * @param datas
     * @param baseConfig
     * @param <T>
     */
    public static <T> void createTbody(Workbook wb, Sheet sheet, List<ExcelColInfo> colConfigs, List<T> datas, ExcelConfig baseConfig) {
        if (datas == null) {
            return;
        }
        for (int j = 0; j < datas.size(); j++) {
            T data = datas.get(j);
            //创建行
            Row rowBody = createRow(sheet, j + 2, baseConfig.getRowHeight());
            if (baseConfig.isHasColIndex()) {
                createCell(baseConfig.getCommonCellStyle(wb), rowBody, 0, (j + 1) + "");
            }
            for (int i = 1; i < colConfigs.size() + 1; i++) {
                ExcelColInfo colConfig = colConfigs.get(i - 1);
                createCell(baseConfig.getCommonCellStyle(wb), rowBody, i, getPropertyValue(data, colConfig.getProperty()));
            }
        }

    }
}
