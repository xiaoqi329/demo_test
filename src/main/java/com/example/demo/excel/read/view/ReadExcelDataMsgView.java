package com.example.demo.excel.read.view;

import com.example.demo.excel.enums.ExceptionType;

/**
 * 错误信息view
 */
public class ReadExcelDataMsgView {
    private String msg;
    private ExceptionType exceptionType;
    private Integer rows;
    private String headName;

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public String getHeadName() {
        return headName;
    }

    public void setHeadName(String headName) {
        this.headName = headName;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public ExceptionType getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(ExceptionType exceptionType) {
        this.exceptionType = exceptionType;
    }

    public ReadExcelDataMsgView(String msg, int rows, String headName, ExceptionType exceptionType) {
        this.msg = msg;
        this.exceptionType = exceptionType;
        this.rows = rows;
        this.headName = headName;
    }

    public ReadExcelDataMsgView(String msg, ExceptionType exceptionType) {
        this.msg = msg;
        this.exceptionType = exceptionType;
    }
}
