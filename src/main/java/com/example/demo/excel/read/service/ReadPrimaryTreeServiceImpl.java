package com.example.demo.excel.read.service;

import com.example.demo.excel.enums.ExceptionType;
import com.example.demo.excel.model.ExcelColInfo;
import com.example.demo.excel.read.model.IReadConfig;
import com.example.demo.excel.read.model.ReadExcelColConfig;
import com.example.demo.excel.read.model.ReadExcelPrimaryConfig;
import com.example.demo.excel.read.model.ReadExcelPrimaryTreeConfig;
import com.example.demo.excel.read.view.ReadExcelDataMsgView;
import com.example.demo.excel.read.view.ReadExcelDataView;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.demo.excel.ExcelUtil.*;

/**
 * 获取树结构表格并生成主键 业务类
 */
public class ReadPrimaryTreeServiceImpl<T> implements IReadService<T> {

    @Override
    public ReadExcelDataView<T> getDataView(MultipartFile file, IReadConfig<T> config) {
        ReadExcelPrimaryTreeConfig<T> treeConfig = (ReadExcelPrimaryTreeConfig<T>) config;
        return readTreeExcelAndGeneratePrimary(file, treeConfig);
    }

    /**
     * 读取excel 数据，并生成主键
     *
     * @param file   文件对象
     * @param config 配置
     */
    public <T> ReadExcelDataView<T> readTreeExcelAndGeneratePrimary(MultipartFile file, ReadExcelPrimaryTreeConfig<T> config) {
        //校验文件
        ReadExcelDataView<T> x = validateFile(file);
        if (x != null) return x;
        try {
            //处理数据
            return readTreeExcelAndGeneratePrimaryMore03(file.getInputStream(), config);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ReadExcelDataView<T>(Collections.singletonList(new ReadExcelDataMsgView("服务器错误，请联系管理员", ExceptionType.SERVER_EXCEPTION)));
    }

    private <T> ReadExcelDataView<T> readTreeExcelAndGeneratePrimaryMore03(InputStream inputStream, ReadExcelPrimaryTreeConfig<T> config) {
        //获取工作簿
        Workbook workbook = getWorkbook(inputStream);
        //校验是否有sheet
        validateHasSheet(workbook);
        //工作表对象 默认读取第一个sheet
        Sheet sheet = workbook.getSheetAt(0);
        //获取excel表中的相关信息
        ReadExcelDataView<T> dataView = getDataViewForTreePrimary(sheet, config);
        //校验是否有填写错误
        validateHasException(dataView);
        return dataView;
    }


    /**
     * 获取 树结构 生成主键dataView
     *
     * @param sheet  sheet
     * @param config 配置
     * @param <T>    泛型
     * @return view
     */
    private <T> ReadExcelDataView<T> getDataViewForTreePrimary(Sheet sheet, ReadExcelPrimaryTreeConfig<T> config) {
        //获取子节点配置
        ReadExcelColConfig sonCol = config.getSonCol();
        //获取父节点配置
        ReadExcelColConfig parentCol = config.getParentCol();
        //父节点 key :主键id;value :parent 值
        Map<String, String> sonPrimaryAndParentIdMap = new HashMap<>();
        Map<String, String> parentIdAndParentPrimaryMap = new HashMap<>();
        //初始化返回对象
        ReadExcelDataView<T> view = new ReadExcelDataView<T>();
        //列配置列表
        List<ExcelColInfo> colConfigs = config.getColConfigs();
        //列数
        int rowLength = sheet.getLastRowNum() + 1;
        //列开始索引
        int xIndex = config.isHasIndex() ? 1 : 0;
        //行开始索引
        int yIndex = config.isHasHead() ? 1 : 0;
        //遍历获取所有的cell 中的内容以及检验数据填写是否正常（外循环每一行，内循环每一列）
        for (int i = yIndex; i < rowLength; i++) {
            Row row = sheet.getRow(i);
            //校验是否是空行
            if (validateRowIsNull(row, view, i + 1)) {
                T model = getNewModel(config.getModel());
                //设置主键值
                String primaryVal = setPrimaryVal(model, config.getPrimaryConfig());
                for (int j = xIndex; j < colConfigs.size() + xIndex; j++) {
                    Cell cell = row.getCell(j);
                    ExcelColInfo colConfig = colConfigs.get(j - xIndex);
                    //获取每个cell中的数据
                    getCellDataForTreePrimary(colConfig, cell, view, model, i + 1, colConfig.getName(), sonCol, parentCol, sonPrimaryAndParentIdMap, parentIdAndParentPrimaryMap, primaryVal);
                }
                view.getDatas().add(model);
            }
        }
        handlerPid(config, view, sonPrimaryAndParentIdMap, parentIdAndParentPrimaryMap);
        return view;
    }

    /**
     * 处理 业务的pid(根据两个map 一个map key 表中son字段value表中pid字段 另一个map 表中key )
     *
     * @param config
     * @param view
     * @param sonPrimaryAndParentIdMap
     * @param parentIdAndParentPrimaryMap
     * @param <T>
     */
    private <T> void handlerPid(ReadExcelPrimaryTreeConfig<T> config, ReadExcelDataView<T> view, Map<String, String> sonPrimaryAndParentIdMap, Map<String, String> parentIdAndParentPrimaryMap) {
        String primaryProperty = config.getPrimaryConfig().getPrimaryProperty();
        for (T data : view.getDatas()) {
            try {
                Field son = data.getClass().getDeclaredField(primaryProperty);
                son.setAccessible(true);
                String id = son.get(data).toString();
                String parentId = sonPrimaryAndParentIdMap.get(id);
                String pid = parentIdAndParentPrimaryMap.get(parentId);
                Field parent = data.getClass().getDeclaredField(config.getParentCol().getProperty());
                parent.setAccessible(true);
                parent.set(data, pid);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private <T> void getCellDataForTreePrimary(ExcelColInfo colConfig, Cell cell, ReadExcelDataView<T> view, T model, int rows, String headName, ExcelColInfo sonCol,
                                               ExcelColInfo parentCol, Map<String, String> sonIdAndParentIdMap, Map<String, String> parentIdAndParentPrimaryMap, String primaryVal) {
        Object returnObj = getCellDataObject(colConfig, cell, view, rows, headName, colConfig.getCustomCellType());
        if (returnObj != null && !colConfig.equals(parentCol)) {
            //反射设置属性值
            try {
                Field declaredField = model.getClass().getDeclaredField(colConfig.getProperty());
                declaredField.setAccessible(true);
                declaredField.set(model, returnObj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (colConfig.equals(parentCol)) {
            sonIdAndParentIdMap.put(primaryVal, returnObj + "");
        } else if (colConfig.equals(sonCol)) {
            parentIdAndParentPrimaryMap.put(returnObj + "", primaryVal);
        }

    }


}
