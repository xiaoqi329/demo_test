package com.example.demo.excel.read.model;

import com.example.demo.excel.read.service.IReadService;
import com.example.demo.excel.read.view.ReadExcelDataView;
import org.springframework.web.multipart.MultipartFile;

/**
 * 读取配置接口
 */
public interface IReadConfig<T> {
    /**
     * 读取对应的业务类
     *
     * @return service
     */
    IReadService<T> getService();
}
