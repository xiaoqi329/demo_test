package com.example.demo.excel.read.service;

import com.example.demo.excel.read.model.IReadConfig;
import com.example.demo.excel.read.view.ReadExcelDataView;
import org.springframework.web.multipart.MultipartFile;

public interface IReadService<T> {
    /**
     * 获取读取的数据信息 包括execl中的内容和填写问题
     *
     * @return view
     */
    ReadExcelDataView<T> getDataView(MultipartFile file, IReadConfig<T> config);
}
