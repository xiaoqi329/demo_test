package com.example.demo.excel.read.view;

import com.example.demo.excel.enums.ExceptionType;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * excel表格所有读取信息view
 *
 * @param <T>
 */
public class ReadExcelDataView<T> {
    private List<ReadExcelDataMsgView> msgs = new ArrayList<>();
    private List<T> datas = new ArrayList<>();

    public List<ReadExcelDataMsgView> getMsgs() {
        return msgs;
    }

    public void setMsgs(List<ReadExcelDataMsgView> msgs) {
        this.msgs = msgs;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public List<String> getExceptionMsgList() {
        List<String> exceptionList = new ArrayList<>();
        for (ReadExcelDataMsgView msg : msgs) {
            if (msg.getRows() != null) {
                //如果是单元格错误
                exceptionList.add("第" + msg.getRows() + "行," + msg.getHeadName() + ExceptionType.getNameForVal(msg.getExceptionType()));
            } else {
                //不是单元格问题
                exceptionList.add(msg.getMsg());
            }
        }
        return exceptionList;
    }

    public String getExceptionMsg() {
        return StringUtils.join(getExceptionMsgList(), "\n");
    }

    public ReadExcelDataView() {
    }

    public ReadExcelDataView(List<ReadExcelDataMsgView> msgs) {
        this.msgs = msgs;
    }
}
