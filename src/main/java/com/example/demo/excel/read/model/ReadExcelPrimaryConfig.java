package com.example.demo.excel.read.model;

import com.example.demo.excel.ExcelUtil;
import com.example.demo.excel.model.ExcelColInfo;
import com.example.demo.excel.read.service.IReadService;
import com.example.demo.excel.read.service.ReadPrimaryServiceImpl;
import com.example.demo.excel.read.view.ReadExcelDataView;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 读取excel （包含主键配置）总 基本配置
 * *
 */
public class ReadExcelPrimaryConfig<T> extends ReadExcelConfig<T> {
    /**
     * 主键配置信息
     */
    private PrimaryConfig primaryConfig;

    public PrimaryConfig getPrimaryConfig() {
        return primaryConfig;
    }

    public void setPrimaryConfig(PrimaryConfig primaryConfig) {
        this.primaryConfig = primaryConfig;
    }


    public ReadExcelPrimaryConfig(boolean hasHead, boolean hasIndex, List<ExcelColInfo> colConfigs, T model, PrimaryConfig primaryConfig) {
        super(hasHead, hasIndex, colConfigs, model);
        this.primaryConfig = primaryConfig;
    }

    @Override
    public IReadService<T> getService() {
        return new ReadPrimaryServiceImpl<T>();
    }
}
