package com.example.demo.excel.read.service;

import com.example.demo.excel.ExcelUtil;
import com.example.demo.excel.read.model.IReadConfig;
import com.example.demo.excel.read.model.ReadExcelPrimaryConfig;
import com.example.demo.excel.read.view.ReadExcelDataView;
import org.springframework.web.multipart.MultipartFile;

/**
 * 自动获取表格信息和生成表格对应主键业务类
 *
 * @param <T>
 */
public class ReadPrimaryServiceImpl<T> extends ReadServiceImpl<T> {

    @Override
    public ReadExcelDataView<T> getDataView(MultipartFile file, IReadConfig<T> config) {
        ReadExcelPrimaryConfig<T> primaryConfig = (ReadExcelPrimaryConfig<T>) config;
        ReadExcelDataView<T> dataView = super.getDataView(file, config);
        for (T data : dataView.getDatas()) {
            ExcelUtil.setPrimaryVal(data, primaryConfig.getPrimaryConfig());
        }
        return dataView;
    }
}
