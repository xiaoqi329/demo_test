package com.example.demo.excel.read.model;

/**
 * 主键配置 需要生成主键时配置信息
 * （此类现阶段主要配置主键的名称和是否生成主键）
 */
public class PrimaryConfig {
    /**
     * 主键属性值
     */
    private String primaryProperty;
    /**
     * 是否自动生成
     */
    private boolean autoGenerate;

    public String getPrimaryProperty() {
        return primaryProperty;
    }

    public void setPrimaryProperty(String primaryProperty) {
        this.primaryProperty = primaryProperty;
    }

    public boolean isAutoGenerate() {
        return autoGenerate;
    }

    public void setAutoGenerate(boolean autoGenerate) {
        this.autoGenerate = autoGenerate;
    }

    public PrimaryConfig(String primaryProperty, boolean autoGenerate) {
        this.primaryProperty = primaryProperty;
        this.autoGenerate = autoGenerate;
    }
}
