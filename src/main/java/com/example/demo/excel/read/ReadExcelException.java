package com.example.demo.excel.read;

/**
 * 自定义读取excel异常
 */
public class ReadExcelException extends RuntimeException {

    public ReadExcelException(String message) {
        super(message);
    }

    public ReadExcelException(String message, Throwable cause) {
        super(message, cause);
    }
}
