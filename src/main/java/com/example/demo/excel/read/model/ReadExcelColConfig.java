package com.example.demo.excel.read.model;

import com.example.demo.excel.enums.CustomCellType;
import com.example.demo.excel.model.ExcelColInfo;

/**
 * 每列的配置信息
 */
public class ReadExcelColConfig extends ExcelColInfo {


    public ReadExcelColConfig(String name, String property, boolean canBlank, Integer maxLength, CustomCellType customCellType) {
        super(canBlank, maxLength, customCellType);
        super.setName(name);
        super.setProperty(property);
    }

    public ReadExcelColConfig() {
    }
}
