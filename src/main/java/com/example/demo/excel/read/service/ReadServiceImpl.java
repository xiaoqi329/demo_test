package com.example.demo.excel.read.service;

import com.example.demo.excel.enums.ExceptionType;
import com.example.demo.excel.read.model.IReadConfig;
import com.example.demo.excel.read.model.ReadExcelConfig;
import com.example.demo.excel.read.view.ReadExcelDataMsgView;
import com.example.demo.excel.read.view.ReadExcelDataView;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

import static com.example.demo.excel.ExcelUtil.*;

/**
 * 基本读取业务类
 */
public class ReadServiceImpl<T> implements IReadService<T> {

    @Override
    public ReadExcelDataView<T> getDataView(MultipartFile file, IReadConfig<T> config) {
        ReadExcelConfig<T> readExcelConfig = (ReadExcelConfig<T>) config;
        return readSimpleExcel(file, readExcelConfig);
    }


    /**
     * @param file   文件对象
     * @param config 配置
     * @return view
     */
    public <T> ReadExcelDataView<T> readSimpleExcel(MultipartFile file, ReadExcelConfig<T> config) {
        //校验文件
        ReadExcelDataView<T> x = validateFile(file);
        if (x != null) return x;
        try {
            //处理数据
            ReadExcelDataView<T> dataView = readSimpleExcelMore03(file.getInputStream(), config);
            //校验是否有填写错误
            validateHasException(dataView);
            return dataView;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ReadExcelDataView<T>(Collections.singletonList(new ReadExcelDataMsgView("服务器错误，请联系管理员", ExceptionType.SERVER_EXCEPTION)));
    }

    /**
     * 读取简单的excel表格的数据(2003及以上版本)
     *
     * @param inputStream 输入流
     * @param config      表格的相关配置
     * @param <T>         泛型返回的数据列表元素
     * @return view 填写错误和数据列表
     * @author qi
     */

    private <T> ReadExcelDataView<T> readSimpleExcelMore03(InputStream inputStream, ReadExcelConfig<T> config) {
        //获取工作簿
        Workbook workbook = getWorkbook(inputStream);
        //校验是否有sheet
        validateHasSheet(workbook);
        //工作表对象 默认读取第一个sheet
        Sheet sheet = workbook.getSheetAt(0);
        //获取excel表中的相关信息
        return getDataViewForSimple(sheet, config);
    }

    /**
     * 从sheet 中获取所有的信息
     *
     * @param sheet  sheet
     * @param config 配置信息
     * @param <T>    泛型
     * @return view
     * @author qi
     */
    public <T> ReadExcelDataView<T> getDataViewForSimple(Sheet sheet, ReadExcelConfig<T> config) {
        //列数
        int rowLength = sheet.getLastRowNum() + 1;
        //列开始索引
        int xIndex = config.isHasIndex() ? 1 : 0;
        //行开始索引
        int yIndex = config.isHasHead() ? 1 : 0;
        return getTableData(sheet, config.getModel(), config.getColConfigs(), rowLength, xIndex, yIndex);

    }

}
