package com.example.demo.excel.read;

import com.example.demo.excel.read.model.IReadConfig;
import com.example.demo.excel.read.view.ReadExcelDataView;
import org.springframework.web.multipart.MultipartFile;

public class ReadFactory<T> {
    /**
     * excel配置
     */
    private IReadConfig<T> readConfig;
    /**
     * excel文件
     */
    private MultipartFile file;

    public ReadFactory(IReadConfig<T> readConfig, MultipartFile file) {
        this.readConfig = readConfig;
        this.file = file;
    }

    public ReadExcelDataView<T> createDataView() {
        return readConfig.getService().getDataView(file, readConfig);
    }
}
