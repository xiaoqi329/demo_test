package com.example.demo.excel.read.model;

import com.example.demo.excel.ExcelUtil;
import com.example.demo.excel.model.ExcelColInfo;
import com.example.demo.excel.read.service.IReadService;
import com.example.demo.excel.read.service.ReadPrimaryTreeServiceImpl;
import com.example.demo.excel.read.view.ReadExcelDataView;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 读取excel 树类型（包含主键配置）总 基本配置 （只适用于自主生成主键的形式，数据生成主键无法支持）
 */
public class ReadExcelPrimaryTreeConfig<T> extends ReadExcelPrimaryConfig<T> implements IReadConfig<T> {
    /**
     * 子节点关系对应的列
     */
    private ReadExcelColConfig sonCol;
    /**
     * 父节点关系对应的列
     */
    private ReadExcelColConfig parentCol;

    public ReadExcelColConfig getSonCol() {
        return sonCol;
    }

    public void setSonCol(ReadExcelColConfig sonCol) {
        this.sonCol = sonCol;
    }

    public ReadExcelColConfig getParentCol() {
        return parentCol;
    }

    public void setParentCol(ReadExcelColConfig parentCol) {
        this.parentCol = parentCol;
    }

    public ReadExcelPrimaryTreeConfig(ReadExcelColConfig sonCol, ReadExcelColConfig parentCol, List<ExcelColInfo> objects, T model, boolean hasIndex, boolean hasHead,
                                      PrimaryConfig primaryConfig) {
        super(hasHead, hasIndex, objects, model, primaryConfig);
        this.sonCol = sonCol;
        this.parentCol = parentCol;
    }

    @Override
    public IReadService<T> getService() {
        return new ReadPrimaryTreeServiceImpl<T>();
    }
}
