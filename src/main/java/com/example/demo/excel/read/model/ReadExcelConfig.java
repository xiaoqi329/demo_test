package com.example.demo.excel.read.model;

import com.example.demo.excel.model.ExcelColInfo;
import com.example.demo.excel.read.service.IReadService;
import com.example.demo.excel.read.service.ReadServiceImpl;

import java.util.List;

/**
 * 读取excel总 基本配置
 */
public class ReadExcelConfig<T> implements IReadConfig<T> {
    /**
     * 是否有表头，默认有
     */
    private boolean hasHead = true;
    /**
     * 是否有序号，默认有
     */
    private boolean hasIndex = true;
    /**
     * 列配置列表 顺序必须和excel头部顺序一致
     */
    private List<ExcelColInfo> colConfigs;
    /**
     * 返回数据的模型
     */
    private T model;

    public boolean isHasIndex() {
        return hasIndex;
    }

    public void setHasIndex(boolean hasIndex) {
        this.hasIndex = hasIndex;
    }

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public boolean isHasHead() {
        return hasHead;
    }

    public void setHasHead(boolean hasHead) {
        this.hasHead = hasHead;
    }

    public List<ExcelColInfo> getColConfigs() {
        return colConfigs;
    }

    public void setColConfigs(List<ExcelColInfo> colConfigs) {
        this.colConfigs = colConfigs;
    }

    public ReadExcelConfig(boolean hasHead, boolean hasIndex, List<ExcelColInfo> colConfigs, T model) {
        this.hasHead = hasHead;
        this.hasIndex = hasIndex;
        this.colConfigs = colConfigs;
        this.model = model;
    }

    @Override
    public IReadService<T> getService() {
        return new ReadServiceImpl<T>();
    }
}