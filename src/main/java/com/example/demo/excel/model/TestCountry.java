package com.example.demo.excel.model;

public class TestCountry {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TestCountry(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
