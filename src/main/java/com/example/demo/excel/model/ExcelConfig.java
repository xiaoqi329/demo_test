package com.example.demo.excel.model;

import com.example.demo.excel.ExcelUtil;
import org.apache.poi.ss.usermodel.*;

/**
 * 基本配置信息
 */
public class ExcelConfig {

    /**
     * 字体大小，默认12
     */
    private int fontSize = 12;

    private String fontName = "宋体";
    /**
     * 行高，默认24
     */
    private int rowHeight = 18;
    /**
     * 所有列宽默认50，列配置中可单独配置单列
     */
    private int columnWidth = 20;

    /**
     * 名称
     */
    private String title;
    /**
     * 通用的单元格样式
     */
    private CellStyle commonCellStyle;
    /**
     * 通用的表头单元格样式
     */
    private CellStyle headCellStyle;
    /**
     * 禁用的单元格样式
     */
    private CellStyle disableCellStyle;

    public CellStyle getDisableCellStyle(Workbook wb,Sheet sheet) {
        Font font = ExcelUtil.createFont(this.getFontSize(), this.getFontName(), wb, false, false);
        disableCellStyle = ExcelUtil.createCellStyle(wb, HorizontalAlignment.CENTER, VerticalAlignment.TOP, true, font, IndexedColors.GREY_40_PERCENT.getIndex());
        disableCellStyle.setLocked(true);
        return disableCellStyle;
    }

    public CellStyle getHeadCellStyle(Workbook wb) {
        if (headCellStyle == null) {
            Font font = ExcelUtil.createFont(this.getFontSize(), this.getFontName(), wb, true, false);
            headCellStyle = ExcelUtil.createCellStyle(wb, HorizontalAlignment.CENTER, VerticalAlignment.CENTER, true, font, IndexedColors.LIGHT_TURQUOISE1.getIndex());
            headCellStyle.setBorderTop(BorderStyle.MEDIUM);
            headCellStyle.setBorderBottom(BorderStyle.MEDIUM);
            headCellStyle.setBorderRight(BorderStyle.MEDIUM);
            headCellStyle.setBorderLeft(BorderStyle.MEDIUM);
        }
        return headCellStyle;
    }

    public CellStyle getCommonCellStyle(Workbook wb) {
        if (commonCellStyle == null) {
            Font font = ExcelUtil.createFont(this.getFontSize(), this.getFontName(), wb, false, false);
            commonCellStyle = ExcelUtil.createCellStyle(wb, HorizontalAlignment.CENTER, VerticalAlignment.TOP, true, font, IndexedColors.LIGHT_GREEN.getIndex());
            commonCellStyle.setBorderTop(BorderStyle.THIN);
            commonCellStyle.setBorderBottom(BorderStyle.THIN);
            commonCellStyle.setBorderRight(BorderStyle.THIN);
            commonCellStyle.setBorderLeft(BorderStyle.THIN);
        }
        return commonCellStyle;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }


    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public int getRowHeight() {
        //限制行高必须大于自高
        return rowHeight > fontSize + 1 ? rowHeight : fontSize + 1;
    }

    public void setRowHeight(int rowHeight) {
        this.rowHeight = rowHeight;
    }

    public int getColumnWidth() {
        return columnWidth;
    }

    public void setColumnWidth(int columnWidth) {
        columnWidth = limit255Char(columnWidth);
        this.columnWidth = columnWidth;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    protected ExcelConfig() {
    }

    public ExcelConfig(int fontSize, String fontName, int rowHeight, int columnWidth, String title) {
        this.fontSize = limitMin(8, fontSize);
        this.fontName = fontName;
        this.rowHeight = limitMin(10, rowHeight);
        this.columnWidth = limit255Char(columnWidth);
        this.title = title;
    }

    private int limitMin(int min, int val) {
        return val > min ? val : min;
    }

    private Integer limit255Char(Integer columnWidth) {
        //excel 列宽最大为255个字符，这里做限制
        if (columnWidth != null && columnWidth > 255) {
            columnWidth = 255;
        }
        return columnWidth;
    }

    /**
     * 获取数据开始的行索引，每个继承类通过覆盖更改
     *
     * @return
     */
    public int getDataStartRowIndex() {
        return 0;
    }

    /**
     * 是否有索引类，每个继承类通过记成更改
     *
     * @return
     */
    public boolean isHasColIndex() {
        return false;
    }

}
