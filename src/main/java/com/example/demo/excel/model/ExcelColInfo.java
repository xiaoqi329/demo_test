package com.example.demo.excel.model;

import com.example.demo.excel.enums.CustomCellType;

/**
 * 列基本信息及限制信息
 */
public class ExcelColInfo {

    /**
     * 列名（头部名称）
     */
    private String name;
    /**
     * 对应封装对象的属性
     */
    private String property;

    /**
     * 是否可空
     */
    private boolean canBlank;

    /**
     * 字符串类型 限制最长 默认500
     */
    private Integer maxLength = 500;

    /**
     * 类型 枚举CellType
     */
    private CustomCellType customCellType;
    /**
     * 数字类型 限制最小
     */
    private Double min;
    /**
     * 数字类型限制最大
     */
    private Double max;
    /**
     * 其他的限制条件信息
     */
    private String otherInfo;
    private Integer columnWidth = 20;

    public Integer getColumnWidth() {
        return columnWidth;
    }

    public void setColumnWidth(Integer columnWidth) {
        this.columnWidth = columnWidth;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public boolean isCanBlank() {
        return canBlank;
    }

    public void setCanBlank(boolean canBlank) {
        this.canBlank = canBlank;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public CustomCellType getCustomCellType() {
        return customCellType;
    }

    public void setCustomCellType(CustomCellType customCellType) {
        this.customCellType = customCellType;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public ExcelColInfo() {
    }

    public ExcelColInfo(boolean canBlank, Integer maxLength, CustomCellType customCellType) {
        this.canBlank = canBlank;
        this.maxLength = maxLength;
        this.customCellType = customCellType;
    }

    public ExcelColInfo(boolean canBlank, Integer maxLength, CustomCellType customCellType, Double min, Double max) {
        this.canBlank = canBlank;
        this.maxLength = maxLength;
        this.customCellType = customCellType;
        this.min = min;
        this.max = max;
    }

    public void setNumberLimit(Double min, Double max) {
        this.min = min;
        this.max = max;
    }
}
