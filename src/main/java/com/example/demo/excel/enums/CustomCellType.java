package com.example.demo.excel.enums;

/**
 * 单元格类型
 */
public enum CustomCellType {
    STRING, DOUBLE, DATE, TIME, INT, SELECT, ENUM;
    /**
     * 对应的枚举类
     */
    private Class e;

    public Class getE() {
        return e;
    }

    public void setE(Class e) {
        this.e = e;
    }
}
