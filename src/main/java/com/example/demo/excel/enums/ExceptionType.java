package com.example.demo.excel.enums;

public enum ExceptionType {
    SERVER_EXCEPTION("服务器错误"), FILE_NOT_NULL("上传文件为空"), FILE_FORMAT_FAULT("文件格式有问题"), NOT_NULL("不能为空"),
    NOT_NUMBER("不是正确数字"), NOT_DATE("不是正确格式的日期"), BEYOND_MAXLENGTH("超出最大长度"),
    NOT_SUPPORT("暂不支持excel2003及以下版本"), ROW_NOT_NULL("没有填写任何数据");
    private String name;

    ExceptionType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getNameForVal(ExceptionType type) {
        for (ExceptionType value : values()) {
            if (value.equals(type)) {
                return value.getName();
            }
        }
        return "未知";
    }
}
