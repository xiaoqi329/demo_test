package com.example.demo.excel.template.service;

import com.example.demo.excel.ExcelUtil;
import com.example.demo.excel.enums.CustomCellType;
import com.example.demo.excel.enums.ExceptionType;
import com.example.demo.excel.read.model.ReadExcelConfig;
import com.example.demo.excel.read.view.ReadExcelDataMsgView;
import com.example.demo.excel.read.view.ReadExcelDataView;
import com.example.demo.excel.template.model.ITemplateConfig;
import com.example.demo.excel.template.model.TemplateColConfig;
import com.example.demo.excel.template.model.TemplateConfig;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

import static com.example.demo.excel.ExcelUtil.*;
import static com.example.demo.excel.ExcelUtil.setLimitForCols;

public class TemplateServiceImpl<T> implements ITemplateService<T> {

    @Override
    public Workbook getWbByConfig(ITemplateConfig<T> config) {
        TemplateConfig<T> templateConfig = (TemplateConfig<T>) config;
        Workbook wb = createWb();
        Sheet sheet = createSheet(wb, templateConfig.getTitle());
        //设置默认宽度
        setDefaultCellWidthAndHeightAndStyle(templateConfig.isHasColIndex(), templateConfig.getColConfigList(), sheet, templateConfig);
        //创建提示信息
        createInformation(sheet, templateConfig);
        //创建表头
        createHeadRowForTemplate(wb, sheet, templateConfig);
        //创建表体
        createTbodyForTemplate(wb, sheet, templateConfig);
        //限制每个列输入格式
        setLimitForCols(sheet, templateConfig, templateConfig.getColConfigList());
        return wb;
    }

    /**
     * 创建表体内容
     *
     * @param wb             工作簿
     * @param sheet          sheet
     * @param templateConfig 配置
     */
    private void createTbodyForTemplate(Workbook wb, Sheet sheet, TemplateConfig<T> templateConfig) {
        ExcelUtil.createTbody(wb, sheet, templateConfig.getColConfigList(), templateConfig.getDatas(), templateConfig);
    }

    @Override
    public ReadExcelDataView<T> getDataView(ITemplateConfig<T> config, MultipartFile file) {
        TemplateConfig<T> templateConfig = (TemplateConfig<T>) config;
        //校验文件
        ReadExcelDataView<T> x = validateFile(file);
        if (x != null) return x;
        try {
            //处理数据
            InputStream inputStream = file.getInputStream();
            //获取工作簿
            Workbook workbook = getWorkbook(inputStream);
            //校验是否有sheet
            validateHasSheet(workbook);
            //工作表对象 默认读取第一个sheet
            Sheet sheet = workbook.getSheetAt(0);
            //获取excel表中的相关信息
            //列数
            int rowLength = sheet.getLastRowNum() + 1;
            //列开始索引
            int xIndex = templateConfig.isHasColIndex() ? 1 : 0;
            //行开始索引
            int yIndex = templateConfig.getDataStartRowIndex();
            ReadExcelDataView<T> dataView = getTableData(sheet, templateConfig.getModel(), templateConfig.getColConfigList(), rowLength, xIndex, yIndex);
            //校验是否有填写错误
            validateHasException(dataView);
            return dataView;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ReadExcelDataView<T>(Collections.singletonList(new ReadExcelDataMsgView("服务器错误，请联系管理员", ExceptionType.SERVER_EXCEPTION)));
    }


    /**
     * 创建导入提示信息
     *
     * @param sheet  sheet
     * @param config 配置
     */
    private void createInformation(Sheet sheet, TemplateConfig config) {
        //创建行，此处行高给固定的2倍
        Row row = createRow(sheet, 0, config.getRowHeight() * 2);
        //创建提示信息单元格样式
        CellStyle informationCellStyle = getInformationCellStyle(sheet, config);
        //创建提示单元格
        createMergeCell(0, 0, 0, config.getXRight(), sheet, row, CustomCellType.STRING, config.getInformation(), informationCellStyle);
    }

    /**
     * 获取提示信息的单元格样式
     *
     * @param sheet  sheet
     * @param config 配置
     * @return cellStyle
     */
    private CellStyle getInformationCellStyle(Sheet sheet, TemplateConfig config) {
        //创建字体样式
        Font font = createFont(config.getFontSize(), config.getFontName(), sheet.getWorkbook(), true, false);
        //创建单元格样式
        return createCellStyle(sheet.getWorkbook(), HorizontalAlignment.CENTER, VerticalAlignment.CENTER, true, font, IndexedColors.LIGHT_YELLOW.getIndex());
    }


    /**
     * 创建表头
     *
     * @param wb     工作簿
     * @param sheet  sheet
     * @param config 配置
     */
    private static void createHeadRowForTemplate(Workbook wb, Sheet sheet, TemplateConfig config) {
        //行索引
        int headRowIndex = 1;
        Row row = createRow(sheet, headRowIndex, config.getRowHeight());
        int startIndex = 0;
        if (config.isHasColIndex()) {
            createCell(config.getHeadCellStyle(wb), row, 0, "序号");
            startIndex = 1;
        }
        for (int i = startIndex; i < config.getColConfigList().size() + startIndex; i++) {
            TemplateColConfig colConfig = (TemplateColConfig) config.getColConfigList().get(i - startIndex);
            createCell(config.getHeadCellStyle(wb), row, i, colConfig.getName());
            //设置列的批注信息
            setHeadColComment(sheet, colConfig, headRowIndex, i);
            //如果列是选择下拉，设置下拉
            setColCellSelect(i, sheet, colConfig);
        }

    }

}
