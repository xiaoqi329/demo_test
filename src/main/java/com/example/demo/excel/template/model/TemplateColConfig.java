package com.example.demo.excel.template.model;

import com.example.demo.excel.enums.CustomCellType;
import com.example.demo.excel.model.ExcelColInfo;

import java.util.List;

/**
 * 模板列配置
 */
public class TemplateColConfig<T> extends ExcelColInfo {
    /**
     * 下拉数据对应的title
     */
    private String selectTitle;

    public String getSelectTitle() {
        return selectTitle;
    }

    public void setSelectTitle(String selectTitle) {
        this.selectTitle = selectTitle;
    }

    /**
     * 如果列的类型为下拉选的话，提供下拉数据（此数据为模型列表）
     */
    private List<T> selects;

    /**
     * 显示在下拉框中的属性，用于生成表格时，某列的每个单元格中下拉框的数据， 泛型中的一种属性（比如说一列的数据是国家，国家的名称是需要显示的属性）
     */
    private String displayProperty;
    /**
     * 读取的属性值，读取用户填完的表格后使用（比如说一列的数据是国家，国家的id是需要读取的属性）
     */
    private String readProperty;


    public String getDisplayProperty() {
        return displayProperty;
    }

    public void setDisplayProperty(String displayProperty) {
        this.displayProperty = displayProperty;
    }

    public String getReadProperty() {
        return readProperty;
    }

    public void setReadProperty(String readProperty) {
        this.readProperty = readProperty;
    }

    public TemplateColConfig(String name, String property, boolean canBlank, CustomCellType cellType) {
        super.setName(name);
        super.setProperty(property);
        super.setCanBlank(canBlank);
        super.setCustomCellType(cellType);
    }


    public List<T> getSelects() {
        return selects;
    }

    public void setSelects(List<T> selects) {
        this.selects = selects;
    }
}
