package com.example.demo.excel.template.service;

import com.example.demo.excel.read.view.ReadExcelDataView;
import com.example.demo.excel.template.model.ITemplateConfig;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

public interface ITemplateService<T> {
    /**
     * 根据配置获取工作簿
     *
     * @param config 配置
     * @return wb 工作簿
     */
    Workbook getWbByConfig(ITemplateConfig<T> config);

    /**
     * 获取excel表数据
     *
     * @param config 配置
     * @param file   文件
     * @return view
     */
    ReadExcelDataView<T> getDataView(ITemplateConfig<T> config, MultipartFile file);

}
