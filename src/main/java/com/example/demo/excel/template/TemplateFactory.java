package com.example.demo.excel.template;

import com.example.demo.excel.read.view.ReadExcelDataView;
import com.example.demo.excel.template.model.ITemplateConfig;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

/**
 * 模板工厂类
 */
public class TemplateFactory<T> {

    private ITemplateConfig<T> config;

    /**
     * 直接传入配置
     *
     * @param config 配置
     */
    public TemplateFactory(ITemplateConfig<T> config) {
        this.config = config;
    }

    /**
     * 根据配置文件获取工作簿
     *
     * @return wb
     */
    public Workbook getWb() {
        return config.getService().getWbByConfig(config);
    }

    /**
     * 获取dataView
     *
     * @param file 文件
     * @return view
     */
    public ReadExcelDataView<T> getDataView(MultipartFile file) {
        return config.getService().getDataView(config, file);
    }

}
