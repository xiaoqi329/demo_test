package com.example.demo.excel.template;

import com.example.demo.excel.enums.CustomCellType;
import com.example.demo.excel.enums.ExceptionType;
import com.example.demo.excel.model.TestCountry;
import com.example.demo.excel.model.TestDemo;
import com.example.demo.excel.read.model.TestAirDemo;
import com.example.demo.excel.template.model.ITemplateConfig;
import com.example.demo.excel.template.model.TemplateColConfig;
import com.example.demo.excel.template.model.TemplateConfig;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TemplateConfigs<T> {

    /**
     * 获取 测试配置
     *
     * @return
     */
    public static ITemplateConfig<TestDemo> getAir() {
        TemplateColConfig<ExceptionType> colConfig = new TemplateColConfig<>("机场名称", "name", false, CustomCellType.ENUM);
        colConfig.setSelects(Arrays.asList(ExceptionType.values()));
        colConfig.setDisplayProperty("name");
        TemplateColConfig colConfig2 = new TemplateColConfig("英文", "engName", false, CustomCellType.STRING);
        colConfig2.setMaxLength(5);
        TemplateColConfig<TestCountry> colConfig3 = new TemplateColConfig<>("国家", "country", false, CustomCellType.SELECT);
        colConfig3.setReadProperty("id");
        colConfig3.setDisplayProperty("name");
        List<TestCountry> testCountries = Arrays.asList(new TestCountry(1L, "中国"), new TestCountry(2L, "日本"), new TestCountry(3L, "美国"));
        colConfig3.setSelects(testCountries);
        TemplateColConfig colConfig4 = new TemplateColConfig("城市", "city", false, CustomCellType.STRING);
        colConfig4.setMaxLength(10);
        TemplateColConfig colConfig5 = new TemplateColConfig("经度", "x", false, CustomCellType.DOUBLE);
        colConfig5.setMin(-90.0);
        colConfig5.setMax(90.0);
        TemplateColConfig colConfig6 = new TemplateColConfig("纬度", "y", false, CustomCellType.DOUBLE);
        colConfig6.setMin(-90.0);
        colConfig6.setMax(90.0);
        TemplateColConfig colConfig7 = new TemplateColConfig("级别", "level", false, CustomCellType.STRING);
        colConfig7.setMaxLength(10);
        TemplateColConfig colConfig8 = new TemplateColConfig("预计飞行里程", "licheng", false, CustomCellType.DOUBLE);
        colConfig8.setMin(0.0);
        return new TemplateConfig<>(Arrays.asList("相关信息1", "相关信息2", "相关信息3"), true, Arrays.asList(colConfig, colConfig2, colConfig3, colConfig4, colConfig5, colConfig6, colConfig7, colConfig8), "sheetName", new TestDemo());
    }

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        CustomCellType anEnum = CustomCellType.ENUM;
        CustomCellType[] values1 = anEnum.values();
        anEnum.setE(ExceptionType.class);
        Method[] declaredMethods = anEnum.getE().getDeclaredMethods();
        System.out.println(declaredMethods);
        Method values = anEnum.getE().getDeclaredMethod("values");
        values.setAccessible(true);
        System.out.println(anEnum.getE());
        CustomCellType auEnum = CustomCellType.STRING;
        System.out.println(auEnum.getE());
    }
}
