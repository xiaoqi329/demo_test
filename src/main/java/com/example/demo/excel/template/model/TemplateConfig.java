package com.example.demo.excel.template.model;

import com.example.demo.excel.enums.CustomCellType;
import com.example.demo.excel.model.ExcelColInfo;
import com.example.demo.excel.model.ExcelConfig;
import com.example.demo.excel.template.service.ITemplateService;
import com.example.demo.excel.template.service.TemplateServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class TemplateConfig<T> extends ExcelConfig implements ITemplateConfig<T> {
    /**
     * 提示信息
     */
    private List<String> informations;
    /**
     * 是否有列序号
     */
    private boolean hasColIndex;
    /**
     * 读取数据封装的模型
     */
    private T model;
    /**
     * 列配置列表信息
     */
    private List<ExcelColInfo> colConfigList;
    /**
     * 导出的数据列表
     */
    private List<T> datas;

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public long getSelectTypeColumnNum() {
        return colConfigList.stream().filter(a -> a.getCustomCellType().equals(CustomCellType.SELECT)).count();
    }

    public List<String> getInformations() {
        return informations;
    }

    public int getXRight() {
        return (this.getColConfigList().size() - 1 + (this.isHasColIndex() ? 1 : 0));
    }

    public String getInformation() {
        StringBuilder sb = new StringBuilder("注意：绿色区域为需要填写的数据，请您务必按照蓝色表头每列的要求填写，否则将无法导入！\r\n");
        for (int i = 0; i < informations.size(); i++) {
            sb.append(i + 1).append(".").append(informations.get(i)).append(";");
        }
        return sb.toString();
    }

    public void setInformations(List<String> informations) {
        this.informations = informations;
    }

    public boolean isHasColIndex() {
        return hasColIndex;
    }

    public void setHasColIndex(boolean hasColIndex) {
        this.hasColIndex = hasColIndex;
    }

    public List<ExcelColInfo> getColConfigList() {
        return colConfigList;
    }

    public void setColConfigList(List<ExcelColInfo> colConfigList) {
        this.colConfigList = colConfigList;
    }

    @Override
    public int getDataStartRowIndex() {
        return 2;
    }

    public TemplateConfig(List<String> informations, boolean hasColIndex, List<ExcelColInfo> colConfigList, String title, T model) {
        this.informations = informations;
        this.hasColIndex = hasColIndex;
        this.colConfigList = colConfigList;
        this.model = model;
        super.setTitle(title);

    }

    public TemplateConfig() {
    }

    /**
     * 提供服务类
     *
     * @return
     */
    @Override
    public ITemplateService getService() {
        return new TemplateServiceImpl();
    }
}
