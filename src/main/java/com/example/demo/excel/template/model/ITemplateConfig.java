package com.example.demo.excel.template.model;

import com.example.demo.excel.template.service.ITemplateService;

/**
 * 配置必须实现的接口，对接哪个业务类
 */
public interface ITemplateConfig<T> {
    /**
     * 根据配置获取对应的业务处理类
     *
     * @return wb
     */
    ITemplateService<T> getService();
}
