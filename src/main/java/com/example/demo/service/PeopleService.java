package com.example.demo.service;

import com.example.demo.mapper.PeopleMapper;
import com.example.demo.model.People;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class PeopleService {

    private PeopleMapper peopleMapper;

    public PeopleService(PeopleMapper peopleMapper) {
        this.peopleMapper = peopleMapper;
    }

    public void test() {
        People people = new People("小红", 18, "大家好");
        try {
            peopleMapper.insertSelective(people);
            System.out.println("插入数据");
            people.setName("小黑");
            peopleMapper.insertSelective(people);
            Thread.sleep(180000);
        } catch (InterruptedException e) {
            System.out.println(123445);
        }

    }
}
