package com.example.demo.common.exception;

/**
 * ResourceConflictException 资源冲突 异常
 *
 * @author zhaogaz
 * @date 2018/06/22
 */
public class ResourceConflictException extends RuntimeException {
    public ResourceConflictException(String msg) {
        super(msg);
    }

    public ResourceConflictException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
