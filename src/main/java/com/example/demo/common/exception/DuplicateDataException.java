package com.example.demo.common.exception;

/**
 * DuplicateDataException 键重复异常
 *
 * @author zhaogaz
 * @date 2018/06/11
 */
public class DuplicateDataException extends RuntimeException {
    public DuplicateDataException() {
        super();
    }

    public DuplicateDataException(String message) {
        super(message);
    }
}
