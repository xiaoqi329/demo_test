package com.example.demo.common.exception;

/**
 * DataOutOfDateException 403 数据过期
 *
 * @author zhaogaz
 * @date 2018/06/11
 */
public class DataOutOfDateException extends RuntimeException {
    public DataOutOfDateException(String msg) {
        super(msg);
    }

    public DataOutOfDateException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
