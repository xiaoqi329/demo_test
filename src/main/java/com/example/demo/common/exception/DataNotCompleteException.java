package com.example.demo.common.exception;

/**
 * 算法调用异常
 * 
 * @author qi
 *
 */
public class DataNotCompleteException extends RuntimeException {

	public DataNotCompleteException(String msg) {
		super(msg);
	}

	public DataNotCompleteException(String msg, Exception e) {
		super(msg + " because of " + e.toString());
	}
}
