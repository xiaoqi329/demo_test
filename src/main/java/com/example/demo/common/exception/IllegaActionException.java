package com.example.demo.common.exception;

/**
 * IllegaActionException 非法操作异常,操作自己不能操作的数据
 *
 * @author zhaogaz
 * @date 2018/06/14
 */
public class IllegaActionException extends RuntimeException {
    public IllegaActionException(String msg) {
        super(msg);
    }

    public IllegaActionException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
