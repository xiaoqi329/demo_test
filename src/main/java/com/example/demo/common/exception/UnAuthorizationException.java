package com.example.demo.common.exception;

/**
 * UnAuthorizationException 未登陆所抛出的异常
 *
 * @author zhaogaz
 * @date 2018/06/19
 */
public class UnAuthorizationException extends RuntimeException {
    public UnAuthorizationException(String msg) {
        super(msg);
    }

    public UnAuthorizationException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
