package com.example.demo.common.exception;

/**
 * 算法调用异常
 * 
 * @author qi
 *
 */
public class AlgorithmFailedException extends RuntimeException {

	public AlgorithmFailedException(String msg) {
		super(msg);
	}

	public AlgorithmFailedException(String msg, Exception e) {
		super(msg + " because of " + e.toString());
	}
}
