package com.example.demo.common.exception;

/**
 * NotFoundException 404找不到异常 
 *
 * @author zhaogaz
 * @date 2018/06/11
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(String msg) {
        super(msg);
    }

    public NotFoundException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
