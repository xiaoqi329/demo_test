package com.example.demo.common.exception;

/**
 * IllegalAuthorizationException 权限验证非法，抛出异常，也就是说，操作的人和修改的人不是一个
 *
 * @author zhaogaz
 * @date 2018/06/11
 */
public class IllegalAuthorizationException extends RuntimeException {
    public IllegalAuthorizationException(String msg) {
        super(msg);
    }

    public IllegalAuthorizationException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
