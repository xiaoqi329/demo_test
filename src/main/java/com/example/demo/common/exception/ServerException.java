package com.example.demo.common.exception;

/**
 * ServerException 服务器内部错误，实在是不知道怎么处理的问题，就抛出该错误
 *
 * @author zhaogaz
 * @date 2018/06/11
 */
public class ServerException extends RuntimeException {

    public ServerException(String msg) {
        super(msg);
    }

    public ServerException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}
