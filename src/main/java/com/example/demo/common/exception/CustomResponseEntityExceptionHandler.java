package com.example.demo.common.exception;

import com.example.demo.excel.read.ReadExcelException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * CustomResponseEntityExceptionHandler 自定义 response
 * 异常处理器，重写了额部分异常方法，同事增加了部分自定义异常
 * 如果是操作人可以自己处理的问题返回状态码 HttpStatus.PRECONDITION_FAILED 202
 * 操作人不能处理的异常 返回 500
 *
 * @author zhaogaz
 * @date 2018/06/11
 */
@RestControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


    /**
     * http 请求体消失，也就是 body缺失,会出现相关错误
     *
     * @param ex      exception
     * @param headers headers
     * @param status  status
     * @param request request
     * @return ResponseEntity
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, ("http message not readable"), headers,
                HttpStatus.BAD_REQUEST, request);
    }

    /**
     * 绑定 方法参数转型不匹配异常（也就是controller方法参数错误），返回common对象，同时提示400错误
     *
     * @param ex exception 捕获的异常
     * @return Common bean
     */
    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handlerMethodArgumentTypeMismatch(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return ("参数错误");
    }

    /**
     * 绑定 主键重复异常，返回错误信息同时提示406不接受提交的参数，unique的约束也会出现这个错误，不一定是主键
     *
     * @param ex exception 捕获的异常
     * @return Common bean
     */
    @ExceptionHandler({DuplicateKeyException.class})
    public ResponseEntity handlerDuplicateKeyException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(("键重复"), HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * 绑定 自定义异常——找不到相关数据，本应该找到，但是没找到的数据，多半都是用户操作问题，返回404，同时返回自定义的消息
     *
     * @param ex exception 捕获的异常
     * @return Common bean
     */
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity handlerNotFoundException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>((ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    /**
     * 绑定 自定义异常——服务器错误，提示500错误，自定义消息参数，实在不可能出现的错误都归结于服务器错误。
     *
     * @param ex exception 捕获的异常
     * @return Common bean
     */
    @ExceptionHandler({ServerException.class})
    public ResponseEntity handlerServerException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>((ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 绑定 验证不通过异常，提示403错误，表示非法操作,禁止该请求
     *
     * @param ex exception 异常
     * @return Common Bean
     */
    @ExceptionHandler({IllegalAuthorizationException.class})
    public ResponseEntity handlerIllegalAuthorization(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>((ex.getMessage()), HttpStatus.FORBIDDEN);
    }

    /**
     * 未登陆异常，提示401
     *
     * @param ex exception 异常
     * @return Common Bean
     */
    @ExceptionHandler({UnAuthorizationException.class})
    public ResponseEntity handlerUnAuthorization(Exception ex) {
        logger.error(ex.getMessage());
        return new ResponseEntity<>((ex.getMessage()), HttpStatus.UNAUTHORIZED);
    }

    /**
     * 未登陆异常，提示401
     *
     * @param ex exception 异常
     * @return Common Bean
     */
    @ExceptionHandler({DataOutOfDateException.class})
    public ResponseEntity handlerDataOutOfDate(Exception ex) {
        logger.error(ex.getMessage());
        return new ResponseEntity<>((ex.getMessage()), HttpStatus.FORBIDDEN);
    }

    /**
     * redis 链接异常
     *
     * @param ex RedisConnectionFailureException
     * @return common response
     */
    @ExceptionHandler({RedisConnectionFailureException.class})
    public ResponseEntity handlerRedisConnectionFailure(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(("redis 链接失败"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 资源冲突异常
     *
     * @param ex ResourceConflictException
     * @return common response
     */
    @ExceptionHandler({ResourceConflictException.class})
    public ResponseEntity handlerResourceConflict(Exception ex) {
        logger.error(ex.getMessage());
        return new ResponseEntity<>((ex.getMessage()), HttpStatus.CONFLICT);
    }

    /**
     * 调用算法失败异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler({AlgorithmFailedException.class})
    public ResponseEntity handlerAlgorithmFailed(Exception ex) {
        logger.error(ex.getMessage());
        return new ResponseEntity<>(("调动算法失败:" + ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 数据未完成异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler({DataNotCompleteException.class})
    public ResponseEntity handlerDataNotCompleteException(Exception ex) {
        logger.warn(ex.getMessage());
        return new ResponseEntity<>((ex.getMessage()), HttpStatus.PRECONDITION_FAILED);
    }

    /**
     * 非法操作异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler({IllegaActionException.class})
    public ResponseEntity handlerIllegaActionException(Exception ex) {
        logger.warn(ex.getMessage());
        return new ResponseEntity((ex.getMessage()), HttpStatus.PRECONDITION_FAILED);
    }

    /**
     * excelExcption
     *
     * @param ex
     * @return
     */
    @ExceptionHandler({ReadExcelException.class})
    public ResponseEntity handlerReadExcelException(Exception ex) {
        logger.warn(ex.getMessage());
        return new ResponseEntity((ex.getMessage()), HttpStatus.PRECONDITION_FAILED);
    }
}
