package com.example.demo.mapper;

import com.example.demo.model.Division;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DivisionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Division record);

    int insertSelective(Division record);

    Division selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Division record);

    int updateByPrimaryKey(Division record);

    /**
     * 获取列表
     * @return list
     */
    List<Division> getList();
}