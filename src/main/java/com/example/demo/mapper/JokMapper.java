package com.example.demo.mapper;

import com.example.demo.model.Jok;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JokMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Jok record);

    int insertSelective(Jok record);

    Jok selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Jok record);

    int updateByPrimaryKey(Jok record);

    /**
     * 批量插入
     *
     * @param list
     */
    void batchInsert(@Param("list") List<Jok> list);

    List<Jok> getList();
}