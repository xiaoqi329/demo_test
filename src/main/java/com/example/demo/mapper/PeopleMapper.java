package com.example.demo.mapper;

import com.example.demo.model.People;
import org.springframework.stereotype.Repository;

@Repository
public interface PeopleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(People record);

    int insertSelective(People record);

    People selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(People record);

    int updateByPrimaryKey(People record);
}