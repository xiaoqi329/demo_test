package com.example.demo;

import com.example.demo.mapper.DivisionMapper;
import com.example.demo.mapper.JokMapper;
import com.example.demo.model.Jok;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wxapi.WxApiCall.WxApiCall;
import com.wxapi.model.RequestModel;
import io.netty.util.internal.StringUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataTest {
    @Autowired
    private JokMapper jokMapper;
    @Autowired
    private DivisionMapper divisionMapper;

    private ObjectMapper objectMapper = new ObjectMapper();
    

    @Test
    public void tests() throws IOException, InterruptedException {
        /*
        for (int i = 60; i < 118.; i++) {
            getListForPage(i + 1);
        }*/

    }

    @Test
    public void updateBlank() {
        List<Jok> list = jokMapper.getList();
        for (Jok jok : list) {
            //System.out.println("1," + jok.getContent());
            String content = jok.getContent().replace("\\t", "").replace("\\n", "").replace("\\r", "");
            //System.out.println("2," + content);
            if (StringUtils.isEmpty(content)) {
                System.err.println(jok.getId());
                jokMapper.deleteByPrimaryKey(jok.getId());
            } else if ("\"".equals(content.trim())) {
                System.err.println(jok.getId());
                jokMapper.deleteByPrimaryKey(jok.getId());
            }
        }
    }

    private void getListForPage(int i) throws IOException, InterruptedException {
        RequestModel model = new RequestModel();
        model.setGwUrl("https://way.jd.com/showapi/wbxh");
        model.setAppkey("02e470ee66210bd6a49575f25a1f1023");
        Map queryMap = new HashMap();
        queryMap.put("time", "2015-07-10"); //访问参数
        queryMap.put("page", i); //访问参数
        queryMap.put("maxResult", "20"); //访问参数
        queryMap.put("showapi_sign", "bd0592992b4d4050bfc927fe7a4db9f3"); //访问参数
        model.setQueryParams(queryMap);
        WxApiCall call = new WxApiCall();
        call.setModel(model);
        int t = 5000;
        String request = call.request();
        if (StringUtils.isEmpty(request)) {
            System.out.println("===========================" + i + "=====================================");
            t += 5000;
        } else {
            JsonNode jsonNode = objectMapper.readTree(request).get("result").get("showapi_res_body").withArray("contentlist");
            for (JsonNode node : jsonNode) {
                JsonNode text = node.get("text");
                List<Jok> list = splitStrs(text);
                try {
                    jokMapper.batchInsert(list);
                } catch (Exception e) {
                    System.out.println("------------------------------------------------------------------------");
                    System.out.println(text);
                }
            }
        }
        Thread.sleep(t);
    }

    private List<Jok> splitStrs(JsonNode text) {
        String pat = "\\d+、";  // 指定好正则表达式
        Pattern p = Pattern.compile(pat);  // 实例化Pattern类
        String s[] = p.split(text.toString());  // 执行拆分操作
        if (s.length == 1) {
            pat = "\\d+.";  // 指定好正则表达式
            p = Pattern.compile(pat);  // 实例化Pattern类
            s = p.split(text.toString());
        }
        if (s.length == 1) {
            String str = text.toString().replace("一", "").replace("二", "")
                    .replace("三", "")
                    .replace("四", "")
                    .replace("五", "")
                    .replace("六", "")
                    .replace("七", "")
                    .replace("八", "")
                    .replace("九", "")
                    .replace("十", "");
            s = str.split("\\\\n、");
        }
        List<Jok> list = new ArrayList<>();
        for (int x = 0; x < s.length; x++) {
            Jok jok = new Jok();
            jok.setContent(s[x]);
            list.add(jok);
        }
        return list;
    }

    @Test
    public void main() {
        String text = "\\t\\t\\t一、有一天，我的绵鞋不见了。 我就说：“不会被老鼠拿去当窝了。” 我弟就说：“姐姐，不可能。” 我问：“为什么？” 我弟说：“你的鞋那么臭，老鼠才不会拿你的鞋走的。” 我说：“那为什么老鼠不拿你的鞋走？” 我弟说：“因为我的比你的臭。” 我顿时无语了。\\n二、遇见一个中国导游。他说从前最头疼的，就是带大团游客到处乱走，必须不停点人头回去找，不然很容易散。但现在他弄了一个随身wifi，免费提供给团员连接。从此游客们都紧紧跟着他，亦步亦趋，生怕离远了没信号，再不必操心队伍走散了。\\n三、小明数学不好被父母转学到一间教会学校。半年后数学成绩全A。妈妈问：“是修女教得好？是教材好？是祷告？……”“都不是，”小明说，“进学校的第一天，我看见一个人被钉死在加号上面，我就知道……他们是玩真的。”\\n四、一次考试全班都考得很差，老师生气的说到：你们觉得自己是智商低的请站起来。很久没同学动，这时小明站起来了，老师问：你承认自己的智商低了？小明说：不是，我智商不低，我只是不忍心看你一个人站着。\\n五、我和老婆是初中同班同学。初一我是正班长，她是副班长。初二我学习成绩跟不上，跟班主任申请，她是正班长，我成了副班长，她性格特好。我妈跟我奶奶关系不好，那时就想如果以后能娶她当老婆，肯定不会跟我妈吵架。现在真的实现了，她对我妈也很好！多修善缘，想什么都能实现！\\n六、一女性朋友，外表打扮男性，平时看起来跟男生无异。大姨妈一直不太正常。几月姨妈不来，到一老中医那儿就诊，老中医一脸慈祥询问病情，朋友说，两个月没来姨妈，结果老中医一脸凝重的说:小伙子，你是来寻开心的吧。\\n七、公交车上听到一男孩对旁边的一女孩说：“谁说不能预测未来，至少我能知道以后我的孩子姓什么，可是你就不同了，你的孩子姓什么还是未知数呢！”那女孩毫无思索地大声回了一句：“哼，那是！但是，我的孩子肯定是我的孩子，你的孩子就未必了哟！”\\n八、今天帮了女同事一个小忙，下班后她说要请我吃饭，我正要说不用客气呢，另一男同事：就男女两个人出去吃饭，让人看到容易误会，最好带上我一起去吧。女同事：那不好吧，和一个男的出去还算正常，带两个男的出去吃饭，让人看到对我的误会会更大的。\\n九、在家打扫卫生，隔壁大妈过来敲门，说她家孙子在过道玩被我家狗咬了。我一脸懵：我没喂狗啊。大妈：别狡辩了，我不止一次在过道听到你喊二狗。我愣了一下，转头冲屋里喊道：二狗，有人找你。老公蓬头垢面走了过来：谁找我。大妈脸涨成猪肝色走了&#8230;&#8230;\\n十、老妈给我电话：“儿子你多久没回家了？我和你爸都挺想你的，你爸说他现在不管做啥眼里都是你的影子。”我顿时热泪盈眶：“我明天就回家，老爸现在在干嘛？”妈：“刚刚在喂驴，现在在喂猪。”我：&#8230;不想回去了\\n十一、一位剩女来到婚姻介绍所，对工作人员说：“我想找的配偶，必须讨人喜欢，有教养，懂礼貌，能说会道，爱说爱笑，喜欢运动，最好还能歌善舞，兴趣广泛，消息灵通……当然，最重要的一条，我希望他能终日在家陪我。我想和他说话，他就开口；我感到厌烦了，他就别出声。”工作人员对她说，“你需要的是一台电视机。”\\n十二、昨天中午有个男同事外出，没把手机带走。他老婆不停地打电话来。午睡的女同事被吵烦了，拿过手机大吼:&#8221;我们在睡觉,你烦不烦！”结果，那位男同事今天到现在都没来上班！\\n十三、有一次我和女友在一起吃零食，我吃了大部分的零食，女朋友没有吃到最后一块，她就生气了；第二次女友吃了大部分的零食，最后一块我没敢吃，她又生气了；第三次女友吃了所有的零食，我吃了最后一块，然后她还是生气了，至今我还是摸不着头脑，搞不懂她到底为啥生气，心中依旧一片凌乱&#8230;&#8230;\\n十四、带哈士奇下小区遛达，迎面走来只金毛，金毛想和二哈玩就拿爪子轻轻的拍了它一下！我也没太在意，谁知二哈当时就躺地上了，四脚朝天！嘴里哼哼唧唧叫一边抽搐。那金毛主人傻眼了委屈的问我：“这&#8230;，你还要讹我还是咋的啊！”路人们都讥讽眼神看着我，小犊子！你给我起来！保证不打死你\\n十五、同学聚会，班中有一男生特别不爷们，有一女生特别彪悍，不过姿色不错。俩人都没有找对象，我们就说你俩好了算了，特娘的男生对女的说，你家有房有车没，特彪悍的女生憋了半天说你能生吗？\\n\\t\\t";
        String str = text.toString().replace("一", "").replace("二", "")
                .replace("三", "")
                .replace("四", "")
                .replace("五", "")
                .replace("六", "")
                .replace("七", "")
                .replace("八", "")
                .replace("九", "")
                .replace("十", "");
        System.out.println(str);
        String[] split = str.split("\\\\n、");
        System.out.println(split.length);
    }
}
