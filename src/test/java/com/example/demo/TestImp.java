package com.example.demo;

import java.util.Arrays;
import java.util.List;

public class TestImp<T> implements ITest<T> {

    private String id;

    private T t;

    private List<String> stringList;


    public TestImp(String id, List<String> strings, T t) {
        this.id = id;
        this.t = t;
        this.stringList = strings;
    }

    public static void main(String[] args) {
        ITest<String> test = new TestImp<>("23", Arrays.asList("22"), "11");
    }

    @Override
    public void test() {

    }
}
