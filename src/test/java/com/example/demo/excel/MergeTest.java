package com.example.demo.excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;

public class MergeTest {

    @Test
    public void test() throws IOException {
        try (Workbook wb = new XSSFWorkbook()) { //or new HSSFWorkbook();
            Sheet sheet = wb.createSheet("new sheet");

            Row row = sheet.createRow((short) 1);
            Cell cell = row.createCell((short) 1);
            cell.setCellValue(new XSSFRichTextString("This is a test of merging"));
            Cell cell2 = row.createCell((short) 2);
            cell2.setCellValue(new XSSFRichTextString("222222222"));
            Row row2 = sheet.createRow((short) 2);
            Cell cell3 = row2.createCell((short) 1);
            cell3.setCellValue(new XSSFRichTextString("222222222"));
            Cell cell4 = row2.createCell((short) 2);
            cell4.setCellValue(new XSSFRichTextString("222222222"));

            sheet.addMergedRegion(new CellRangeAddress(1, 2, 1, 2));

            // Write the output to a file
            try (FileOutputStream fileOut = new FileOutputStream("merging_cells.xlsx")) {
                wb.write(fileOut);
            }
        }
    }

    @Test
    public void mainTest() throws IOException {
        try (Workbook wb = new XSSFWorkbook()) {
            CreationHelper factory = wb.getCreationHelper();
            Sheet sheet = wb.createSheet();
            Cell cell1 = sheet.createRow(3).createCell(5);
            cell1.setCellValue("F4");
            Drawing<?> drawing = sheet.createDrawingPatriarch();
            ClientAnchor anchor = factory.createClientAnchor();
            Comment comment1 = drawing.createCellComment(anchor);
            RichTextString str1 = factory.createRichTextString("Hello, \nWorld!");
            comment1.setString(str1);
            comment1.setAuthor("Apache POI");
            cell1.setCellComment(comment1);
            Cell cell2 = sheet.createRow(2).createCell(2);
            cell2.setCellValue("C3");
            Comment comment2 = drawing.createCellComment(anchor);
            RichTextString str2 = factory.createRichTextString("XSSF can set cell comments");
            //apply custom font to the text in the comment
            Font font = wb.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 14);
            font.setBold(true);
            font.setColor(IndexedColors.RED.getIndex());
            str2.applyFont(font);
            comment2.setString(str2);
            comment2.setAuthor("Apache POI");
            comment2.setAddress(new CellAddress("C3"));
            try (FileOutputStream out = new FileOutputStream("comments.xlsx")) {
                wb.write(out);
            }
        }
    }
}
