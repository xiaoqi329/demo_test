package com.example.demo.excel;

import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;

public class LimitTest {

    public static void main(String[] args) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Data Validation");
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(new String[]{"11", "21", "31"});
        CellRangeAddressList addressList = new CellRangeAddressList(0, 0, 0, 0);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        // Here the boolean value false is passed to the setSuppressDropDownArrow()
        // method. In the hssf.usermodel examples above, the value passed to this
        // method is true.
        validation.setSuppressDropDownArrow(true);
        // Note this extra method call. If this method call is omitted, or if the
        // boolean value false is passed, then Excel will not validate the value the
        // user enters into the cell.
        validation.setShowErrorBox(true);
        sheet.addValidationData(validation);

        //验证数字大小
        CellRangeAddressList addressList2 = new CellRangeAddressList(3, 3, 0, 7);
        dvConstraint = (XSSFDataValidationConstraint) dvHelper.createNumericConstraint(XSSFDataValidationConstraint.ValidationType.INTEGER, XSSFDataValidationConstraint.OperatorType.GREATER_OR_EQUAL, "100", null);
        validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList2);
        validation.setShowErrorBox(true);
        sheet.addValidationData(validation);
        //验证自定义(这玩意是个公式，不大好使)
        CellRangeAddressList addressList3 = new CellRangeAddressList(5, 5, 0, 7);
        dvConstraint = (XSSFDataValidationConstraint) dvHelper.createCustomConstraint("0");
        validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList3);
        validation.setShowErrorBox(true);
        sheet.addValidationData(validation);
        //验证数字烈性
        CellRangeAddressList addressList4 = new CellRangeAddressList(7, 7, 0, 7);
        dvConstraint = (XSSFDataValidationConstraint) dvHelper.createNumericConstraint(XSSFDataValidationConstraint.ValidationType.DECIMAL, XSSFDataValidationConstraint.OperatorType.BETWEEN, "11", (Double.MAX_VALUE / 10000) + "");
        validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList4);
        validation.setShowErrorBox(true);
        sheet.addValidationData(validation);

        //验证日期
        CellRangeAddressList addressList5 = new CellRangeAddressList(9, 9, 0, 7);
        dvConstraint = (XSSFDataValidationConstraint) dvHelper.createDateConstraint(DataValidationConstraint.OperatorType.BETWEEN, "Date(1900,01,01)", "Date(2099,12,30)", "yyyy/MM/dd");
        validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList5);
        validation.setShowErrorBox(true);
        validation.createErrorBox("提示", "请输入[yyyy-MM-dd]格式日期");
        sheet.addValidationData(validation);
        //验证时间
        CellRangeAddressList addressList6 = new CellRangeAddressList(11, 11, 0, 7);
        dvConstraint = (XSSFDataValidationConstraint) dvHelper.createTimeConstraint(DataValidationConstraint.OperatorType.BETWEEN, "Time(12,00,00)", "Time(23,00,00)");
        validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList6);
        validation.setShowErrorBox(true);
        validation.createErrorBox("提示", "请输入[mm:tt]格式日期");
        sheet.addValidationData(validation);
        //验证
        try (FileOutputStream fos = new FileOutputStream("limit.xlsx")) {
            workbook.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
